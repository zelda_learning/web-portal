import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../applicant.service';

@Component({
  selector: 'app-accept-message',
  templateUrl: './accept-message.component.html',
  styleUrls: ['./accept-message.component.css']
})
export class AcceptMessageComponent implements OnInit {

  constructor(
    public applicantsService: ApplicantService
  ) { }

  ngOnInit() {
  }

  sendMessage(bursaryApplication, status: string) {
    console.log('Send message called');
    // todo: send message here
    this.applicantsService
      .updateApplicationStatus(bursaryApplication.applicationFormId, status);
  }
}
