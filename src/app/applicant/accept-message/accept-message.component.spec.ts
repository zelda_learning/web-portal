import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptMessageComponent } from './accept-message.component';

describe('AcceptMessageComponent', () => {
  let component: AcceptMessageComponent;
  let fixture: ComponentFixture<AcceptMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
