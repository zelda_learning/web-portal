import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import {combineLatest, from, Observable, of, Subject} from 'rxjs';
import { Applicant } from '../models/Applicant';
import { ApplicationForm } from '../models/ApplicationForm';


import {filter, flatMap, map, switchMap, take} from 'rxjs/operators';
import { ApplicationSubmitted } from '../models/ApplicationSubmitted';
import {Bursary} from '../models/Bursary';
import {BursaryService} from '../bursary/bursary.service';
import {AuthService} from '../core/services/auth.service';
import {ClientAdmin} from '../models/ClientAdmin';

@Injectable()
export class ApplicantService {
  applicantsCollection: AngularFirestoreCollection<Applicant>;
  applicantsCBACollection: AngularFirestoreCollection<Applicant>;

  ApplicantFormDocument: AngularFirestoreDocument;

  clientAdminDocument: AngularFirestoreDocument;

  applicants: Observable<Applicant[]>;
  ApplicantForm: Observable<ApplicationForm>;

  appSubmittedCollection: AngularFirestoreCollection<ApplicationSubmitted>;
  AppSubmittedDocument: AngularFirestoreDocument<ApplicationSubmitted>;
  AppSubmitted: Observable<ApplicationSubmitted>;

  editIconDisplay: string = 'none';
  clientId: string;
  sortDirectionInc: boolean = true;
  sortDetailsOption: string = ''; // reflects the value of the sort by drop down menu
  titleOne: string = 'aggregateMark';
  titleTwo: string = 'combinedYearlyIncome';
  titleThree: string = 'clientStatus';
  titleFive: string = 'firstName';
  selectedPageTab: string = 'All'; // sets which tab is shown in modal
  searchInputValues: string = '';
  searchDetailsOption: string = '';
  applicantsPreviewFirstTimeOpen: boolean = false;

  acceptMessageModalOpen: boolean = false; // preview modals
  feedbackMessageEditModalOpen: boolean = false; // edit message modal
  feedbackMessageModalTab: string = 'accept';
  rejectMessage: string = '';

  csvData = [];

  selectedApplicant: Applicant = {
    applicationId: '',
    userId: '',
    status: '',
    applicationFormId: '',
    formA: {},
    formB: {  subjectMarksMostRecent : { }},
    formC: {},
    acceptanceMessage: '',
    rejectionMessage: ''
  };

  selectedBursary: Bursary = {
    id: '',
    clientId: '',
    title: '',
    bursaryUrl: '',
    field: [],
    applicableFields: [],
    subtitle: '',
    applicationProcess: '',
    supportProvided: '',
    requirements: '',
    closingDate: new Date(),
    sections: 0,
    internalApplicationForm: true,
  };
  selectedBursaryForm: any = {
    formA: {},
    formB: {},
    formC: {}
  };
  allCounter: number;
  flaggedCounter: number;
  shortlistedCounter: number;
  rejectedCounter: number;
  selectedPreviewTab: string = 'one'; // sets which tab is shown in modal


  daysLeft: number = 0;

  ready = false;
  forms: any[] = [];
  formsSub: Subject<any[]>;

  rejectMessageModalOpen: boolean = false;
  acceptMessage: string = '';
  clientAdmin: ClientAdmin;


  formIds: string[] = [];
  formsLength = 0;
  previewModalOpen: boolean = false; // preview modal shows applicant's application details


  constructor(private afs: AngularFirestore,
              private bursaryService: BursaryService,
              private authService: AuthService) {
    this.formsSub = new Subject<any[]>();

  }


  // fetch all applicants associated with clientId of logged in client admin
  getApplicationForms(bursaryId: string): Observable<Applicant[]> {

    // todo: limit fetch to maybe 10 applicationForms
    this.applicantsCollection = this.afs.collection('ApplicationForm',
      (ref) => ref
        .where('applicationId', '==', bursaryId)
        .where('submitted', '==', true));

    this.applicants = this.applicantsCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(action => {
        const ApplicantData = action.payload.doc.data() as Applicant;
        ApplicantData.applicationFormId = action.payload.doc.id;

        ApplicantData.clientStatus = '';
        ApplicantData.readStatus = 'Unread';
        ApplicantData.flagged = false;



        return ApplicantData;
      });


    }));


    return this.applicants;
  }


  // fetch the applicants forms details
  getUserApplicationForm(id: string, formName: string): Observable<ApplicationForm> {

    this.ApplicantFormDocument = this.afs.doc(`${formName}/${id}`);

    this.ApplicantForm = this.ApplicantFormDocument.valueChanges().pipe(map(data => data));

    return this.ApplicantForm;
  }


  getApplicationForm(id: string): Observable<ApplicationSubmitted> {
    this.AppSubmittedDocument = this.afs.doc<ApplicationSubmitted>(`ApplicationForm/${id}`);
    this.AppSubmitted = this.AppSubmittedDocument.valueChanges().pipe(map(data => {
      if (data) {
        return data as ApplicationSubmitted;
      } else {
        return null;
      }
    }));
    return this.AppSubmitted;
  }


  addApplicant(newApplicant: Applicant) {
    return new Promise((resolve, reject) => {
      this.applicantsCollection.add(newApplicant)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }


  updateApplicationStatus(formId: string, status: string) {
    console.log('Update status called');

    console.log('Update flag status called', formId, status);


    if ( status === 'Shortlisted' ) {
      this.clientAdmin.shortlistedList[formId] = status;
      this.shortlistedCounter += 1;
    } else {
      this.clientAdmin.shortlistedList[formId] = '';

      if ( status === 'unShortlisted' ) {
        this.shortlistedCounter -= 1;
      }

      status = '';
    }

    this.clientAdminDocument = this.afs.doc(`ClientsAdmins/${this.clientAdmin.clientAdminId}`);


    this.clientAdminDocument
      .update({'shortlistedList': this.clientAdmin.shortlistedList })
      .then(res => console.log('res', res))
      .catch(err => console.log('err', err));


    const currentPosition = this.formIds.indexOf(formId);
    this.selectedApplicant = this.forms[currentPosition];
    this.selectedApplicant.clientStatus = status;
    this.selectedApplicant.status = status;

    this.forms[currentPosition] = this.selectedApplicant;

    this.updateApplicationReadStatus(this.selectedApplicant.applicationFormId);


  }


  updateApplicationFlagStatus(formId: string, flaggedStatus: boolean) {
    console.log('Update flag status called', formId, flaggedStatus);


    this.clientAdminDocument = this.afs.doc(`ClientsAdmins/${this.clientAdmin.clientAdminId}`);

    this.clientAdmin.flaggedList[formId] = !flaggedStatus;

    this.clientAdminDocument
      .update({'flaggedList': this.clientAdmin.flaggedList })
      .then(res => console.log('res', res))
      .catch(err => console.log('err', err));


    const currentPosition = this.formIds.indexOf(formId);
    this.selectedApplicant = this.forms[currentPosition];
    this.selectedApplicant.flagged = !flaggedStatus;
    this.forms[currentPosition] = this.selectedApplicant;

    const newFlagStatus = !flaggedStatus;
    if ( newFlagStatus ) {
      this.flaggedCounter += 1;
    } else if (!newFlagStatus) {
      this.flaggedCounter -= 1;

    }

  }


  toggleEditIconDisplay(id, tabTitle) {
    if (tabTitle === 'Rejected' || tabTitle === 'Shortlisted') {
      this.editIconDisplay = 'block';
    } else {
      this.editIconDisplay = 'none';
    }
  }

  updateApplicationReadStatus(formId: string) {
    console.log('Update ReadStatus called');

    this.clientAdmin.readList[formId] = 'Read';

    this.clientAdminDocument = this.afs.doc(`ClientsAdmins/${this.clientAdmin.clientAdminId}`);

    this.clientAdminDocument
      .update({'readList': this.clientAdmin.readList })
      .then(res => res)
      .catch(err => err);


    const currentPosition = this.formIds.indexOf(formId);
    this.selectedApplicant = this.forms[currentPosition];
    this.selectedApplicant.readStatus = 'Read';
    this.forms[currentPosition] = this.selectedApplicant;
  }

  getUserApplicationForms() {
    return this.forms;
  }

  setSelectedBursary(selectedBursary: Bursary) {

    console.log('called with', selectedBursary);
    this.selectedBursary = selectedBursary;
    this.initAppForms();

  }

  initAppForms() {
    console.log(this.selectedBursary.id);
    this.bursaryService.getBursaryForm(this.selectedBursary.id, 'ApplicationFormA').pipe(
      map(formA => formA ? formA.formValues : {})
    ).subscribe(formA => {
      this.selectedBursaryForm.formA = formA;
    });

    this.bursaryService.getBursaryForm(this.selectedBursary.id, 'ApplicationFormB').pipe(
      map(formB => formB ? formB.formValues : {}),
      take(1)
    ).subscribe(formB => {
      this.selectedBursaryForm.formB = formB;
    });

    this.bursaryService.getBursaryForm(this.selectedBursary.id, 'ApplicationFormC').pipe(
      map(formC => formC ? formC.formValues : {}),
      take(1)
    ).subscribe(formC => {
      this.selectedBursaryForm.formC = formC;
    });


    this.authService.getClientAdmin().pipe(
      map(clientAdmin => {
        // console.log(clientAdmin);
        this.clientAdmin = clientAdmin;

        return clientAdmin;
      }),
      switchMap((client: any) => {
        // console.log(client);
        return this.bursaryService.getBursaryMetadata(client.clientId, this.selectedBursary.id);
      })
    ).subscribe(metaData => {


      if (metaData) {
        this.acceptMessage = metaData.acceptMessage;
        this.rejectMessage = metaData.rejectMessage;
      } else {

        this.acceptMessage = '';
        this.rejectMessage = '';
      }

    });

    // todo : add get accept / reject message if not CBA
    let formNumber = 0;
    this.getApplicationForms(this.selectedBursary.id).pipe(
      map(forms => {
        // console.log(forms);
        this.formsLength = forms.length;
        this.allCounter = forms.length;
        return from(forms).pipe(
          map(form => {
            return form;
          })
        );
      }),
      flatMap(form => {
        return form;
      }),
      map(form => {
        return form;
      }),
      filter(form => {
        // console.log(form.applicationFormId);
        if (this.formIds.indexOf(form.applicationFormId) < 0) {
          // console.log(form);
          this.formIds.push(form.applicationFormId);
          this.forms.push(form);
          // formNumber++;
          return true;
        } else {
          // formNumber++;
          if (this.forms[this.formIds.indexOf(form.applicationFormId)] === form) {
            return false;
          } else {
            this.updateObject(this.forms[this.formIds.indexOf(form.applicationFormId)], form);
            return false;
          }
        }
      }),
      map((form: any) => {
        return combineLatest(
          this.getUserApplicationForm(form.applicationFormId, 'UserApplicationFormA')
            .pipe(map(formA => {
              if (formA) {
                // console.log('formA', formA);
                if (formA['address']) {
                  // console.log('address', formA['address']);
                  const address = formA['address'].split('$');
                  const newAddress = address[0] + ', ' + address[1] + ', ' + address[2] + ', ' + address[3] + ', ' + address[4];
                  formA['address'] = newAddress;
                  formA['zipCode'] = address[4];
                }
                return formA;
              } else {
                return {};
              }
            })),
          this.getUserApplicationForm(form.applicationFormId, 'UserApplicationFormB')
            .pipe(map(formB => {

              if (formB) {
                // console.log('formA', formA);
                if (formB['schoolAddress']) {
                  // console.log('address', formA['address']);
                  const address = formB['schoolAddress'].split('$');
                  const newAddress = address[0] + ', ' + address[1] + ', ' + address[2] + ', ' + address[3] + ', ' + address[4];
                  formB['schoolAddress'] = newAddress;
                  formB['zipCode'] = address[4];
                }
                return formB;
              } else {
                return {};
              }

            })),
          this.getUserApplicationForm(form.applicationFormId, 'UserApplicationFormC')
            .pipe(map(formC => formC ? formC : {})),
          of(form)
        ).pipe(
          map(allUserForms => allUserForms),
          take(1)
        );
      }),
      flatMap(userForm => userForm)
    ).subscribe(form => {
      // console.log(form);
      formNumber++;
      this.forms[this.formIds.indexOf(form[3].applicationFormId)].formA = form[0];
      this.forms[this.formIds.indexOf(form[3].applicationFormId)].formB = form[1];
      this.forms[this.formIds.indexOf(form[3].applicationFormId)].formC = form[2];


      if (this.forms[this.formIds.indexOf(form[3].applicationFormId)].formB.subjectMarksMostRecent) {

        const report = this.forms[this.formIds.indexOf(form[3].applicationFormId)].formB.subjectMarksMostRecent;
        const subjects = Object.keys(report);

        let total = 0;
        const average = subjects.length;

        subjects.forEach(subject => {
          total += report[subject] ;

        });

        const mark = Math.floor(total / average);
        this.forms[this.formIds.indexOf(form[3].applicationFormId)].formB.aggregateMark = mark;
      }

      const dataObject = { ...form[0], ...form[1], ...form[2]};

      // todo: school address reformatting
      // todo: check if this is better with a pdf

      if (Object.keys(dataObject).length > 0) {
        this.csvData.push(dataObject);
      }

      if (formNumber === this.formsLength) {
        this.ready = true;
        this.selectedApplicant = this.forms[0];
        this.formsSub.next(this.forms);


        this.shortlistedCounter = 0;
        this.rejectedCounter = 0;
        this.flaggedCounter = 0;

        const shortListedIds = this.objectKeys(this.clientAdmin.shortlistedList);

        if (shortListedIds) {
          shortListedIds.forEach(id => {

            const currentPosition = this.formIds.indexOf(id);
            const applicant = this.forms[currentPosition];
            applicant.readStatus = 'Read';

            if ( this.clientAdmin.shortlistedList[id] === 'Shortlisted') {
              this.shortlistedCounter += 1;

              applicant.clientStatus = 'Shortlisted';
              applicant.status = 'Shortlisted';

              this.forms[currentPosition] = applicant;

            }

          });

        }

        const flaggedIds = this.objectKeys(this.clientAdmin.flaggedList);

        if (flaggedIds) {
          flaggedIds.forEach(id => {

            const currentPosition = this.formIds.indexOf(id);
            const applicant = this.forms[currentPosition];
            applicant.readStatus = 'Read';

            if ( this.clientAdmin.flaggedList[id] === true ) {
              this.flaggedCounter += 1;

              applicant.flagged = true;
              this.forms[currentPosition] = applicant;
            }

          });

        }

        const readIds = this.objectKeys(this.clientAdmin.readList);

        if (readIds) {
          readIds.forEach(id => {

            if ( this.clientAdmin.readList[id] === 'Read' ) {

              const currentPosition = this.formIds.indexOf(id);
              const applicant = this.forms[currentPosition];
              applicant.readStatus = 'Read';
              this.forms[currentPosition] = applicant;
            }

          });

        }


      }
    });
  }


  updateObject(current, updated) {
    Object.keys(updated).forEach(function (key) {
      current[key] = updated[key];
    });
    return updated;
  }

  changeSortDetailsOption(sortOption: string) {
    console.log('This is the sortOption: ', sortOption);
    console.log('This is the sortDirectionInc', this.sortDirectionInc);
    this.sortDetailsOption = sortOption;
    this.sortDirectionInc = !(this.sortDirectionInc);
  }

  daysBetween(date1, date2) {
    console.log(date1, date2, 'dates recieved');
    // Get 1 day in milliseconds
    const one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    const date1_ms = date1.getTime();
    const date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    const difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
  }

  acceptMessageModalToggle(bursary: Bursary) {
    if (bursary) {
      this.selectedBursary = bursary;
      console.log('accept Message modal called with bursary : ');
    }
    this.acceptMessageModalOpen = !this.acceptMessageModalOpen;
    this.previewModalOpen = !this.previewModalOpen;
  }

  // toggles modal, if the modal is bing opened,
  // set bursary variable to be accessed inside modal
  // todo: show spinner and fetch applicant data
  previewModalToggle(application: Applicant) {

    console.log( application);
    this.selectedPreviewTab = 'one';
    this.updateApplicationReadStatus(application.applicationFormId);
    this.selectedApplicant = application;
    this.previewModalOpen = !this.previewModalOpen;

  }



  // triggered whenever a tab is changed in the modal
  changePageTab(tab: string) { this.selectedPageTab = tab; }

  updateSpecificApplicationStatus(bursaryApplication, status: string) {
    this.updateApplicationReadStatus(bursaryApplication.applicationFormId);
    this.updateApplicationStatus(bursaryApplication.applicationFormId, status);

  }

  updateSpecificApplicationFlagStatus(bursaryApplication) {
    // console.log(bursaryApplication);
    this.updateApplicationReadStatus(bursaryApplication.applicationFormId);
    this.updateApplicationFlagStatus(bursaryApplication.applicationFormId, bursaryApplication.flagged);
  }

  evaluateDictProperty(dictionary: any, key: string) {
    if (typeof dictionary === 'undefined' || !(key in dictionary)) {
      return '';
    } else {
      return dictionary[key];
    }
  }


  setApplicantsFirstTimeOpen(bool: boolean) {
    this.applicantsPreviewFirstTimeOpen = bool;
  }


  feedbackMessageEditModalToggle() {
    console.clear();
    console.log('toggle feedback Modal ', this.selectedBursary);
    this.feedbackMessageEditModalOpen = !this.feedbackMessageEditModalOpen;
  }

  viewPreviousApplicant() {

    console.clear();
    console.log(this.selectedApplicant);

    const currentPosition = this.formIds.indexOf(this.selectedApplicant.applicationFormId);
    const newPosition = currentPosition - 1;

    if (newPosition >= 0) {

      this.selectedApplicant = this.forms[newPosition];
      this.updateApplicationReadStatus(this.selectedApplicant.applicationFormId);

    }
  }


  rejectMessageModalToggle(bursary: Bursary) {

    if (bursary) {

      this.selectedBursary = bursary;
      console.log('accept Message modal called with bursary : ');

    }
    this.rejectMessageModalOpen = !this.rejectMessageModalOpen;
    this.previewModalOpen = !this.previewModalOpen;
  }

  changeDefaultOpenMessageEditModal() {
    if (this.selectedPageTab === 'Rejected') {
      this.feedbackMessageModalTab = 'reject';
    } else {
      this.feedbackMessageModalTab = 'accept';
    }
  }

  onSearchKey(event: KeyboardEvent) {
    this.searchInputValues = (<HTMLInputElement>event.target).value;
    console.log(this.searchInputValues);
  }

  changeSearchDetailsOption(e) {
    this.searchDetailsOption = e;

    console.log(e);
  }

  updateAcceptAndRejectMessages() {
    console.clear();
    console.log(this.acceptMessage, this.rejectMessage);
    this.bursaryService
      .updateBursaryMetaData(this.clientAdmin.clientId, this.selectedBursary.id,
        { acceptMessage: this.acceptMessage, rejectMessage: this.rejectMessage});
  }

  objectKeys(obj: object) {

    if (obj) { return Object.keys(obj); }
  }

  changeFeedbackMessageTab( tab: string ) { this.feedbackMessageModalTab = tab; }

  viewNextApplicant() {

    console.clear();

    const currentPosition = this.formIds.indexOf(this.selectedApplicant.applicationFormId);
    const newPosition = currentPosition + 1;

    if (newPosition !== this.formIds.length) {
      // this.selectedApplicant = {...this.forms[newPosition], ...this.bursaryApplications[this.formIds[newPosition]]};
      this.selectedApplicant = this.forms[newPosition];
      this.updateApplicationReadStatus(this.selectedApplicant.applicationFormId);

    }
  }

}

