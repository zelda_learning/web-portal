import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../../applicant.service';

@Component({
  selector: 'app-extra-details',
  templateUrl: './extra-details.component.html',
  styleUrls: ['./extra-details.component.css']
})
export class ExtraDetailsComponent implements OnInit {

  constructor(
    public applicantService: ApplicantService
  ) { }

  ngOnInit() {}

}
