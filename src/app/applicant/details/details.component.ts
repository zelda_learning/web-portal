import {Component, ElementRef, OnInit} from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApplicantService } from '../applicant.service';
import { Applicant } from '../../models/Applicant';
import { ApplicationSubmitted } from '../../models/ApplicationSubmitted';
import { Observable } from 'rxjs';
import { ApplicationForm } from '../../models/ApplicationForm';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  },
})
export class DetailsComponent implements OnInit {


  applicantsPreviewFirstTimeOpen: boolean = false;

  // Get the modal
   modal = document.getElementById('applicantsPreviewId');

  constructor(
    public applicantService: ApplicantService,
    private _eref: ElementRef
  ) { }



  ngOnInit() {

  }

  onClick(event) {
    const modal = document.getElementById('applicantsPreviewId');

    if ( this._eref.nativeElement.contains(event.target) ){

      if (event.target === modal) {
        modal.style.display = 'none';
      }

    }
  }

  onClickOutsideApplicantsPreview() {
    if (this.applicantsPreviewFirstTimeOpen) {
      this.setApplicantsFirstTimeOpen(false);
    } else if ((document.getElementById('applicantsPreviewId').style.display === 'block')) {
      this.applicantService.previewModalToggle(this.applicantService.selectedApplicant);
    }
  }



// When the user clicks anywhere outside of the modal, close it
//   window.onclick = function(event) {
//     if (event.target === this.modal) {
//       this.modal.style.display = 'none';
//     }
//   };

  setApplicantsFirstTimeOpen(bool: boolean) {
    this.applicantsPreviewFirstTimeOpen = bool;
  }

}
