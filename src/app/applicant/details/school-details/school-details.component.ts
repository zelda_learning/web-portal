import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../../applicant.service';

@Component({
  selector: 'app-school-details',
  templateUrl: './school-details.component.html',
  styleUrls: ['./school-details.component.css']
})
export class SchoolDetailsComponent implements OnInit {

  constructor(
    public applicantService: ApplicantService
  ) { }

  ngOnInit() {
  }

}
