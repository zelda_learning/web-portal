import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ApplicantRoutingModule } from './applicant-routing.module';
import { ApplicantComponent } from './applicant.component';
import { DetailsComponent } from './details/details.component';

import { ApplicantsFilterPipe } from './applicantsfilter.pipe';
import { SortingSearchApplicantsPipe } from './sort.pipe';

import { ApplicantService } from './applicant.service';
import {BursaryService} from '../bursary/bursary.service';

import { CKEditorModule } from 'ng2-ckeditor';

import { ClickOutsideModule } from 'ng-click-outside';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule } from '@angular/material';
import { TabsListComponent } from './tabs-list/tabs-list.component';
import { TableHeaderComponent } from './table-header/table-header.component';
import { FilterSearchComponent } from './filter-search/filter-search.component';
import { UnreadApplicationsComponent } from './unread-applications/unread-applications.component';
import { ReadApplicationsComponent } from './read-applications/read-applications.component';
import { RejectedApplicationsComponent } from './rejected-applications/rejected-applications.component';
import { PaginationComponent } from './pagination/pagination.component';
import { FeedbackMessageComponent } from './feedback-message/feedback-message.component';
import { PersonalDetailsComponent } from './details/personal-details/personal-details.component';
import { SchoolDetailsComponent } from './details/school-details/school-details.component';
import { ExtraDetailsComponent } from './details/extra-details/extra-details.component';
import { AcceptMessageComponent } from './accept-message/accept-message.component';
import { RejectMessageComponent } from './reject-message/reject-message.component';
import { EditMessageComponent } from './edit-message/edit-message.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';


@NgModule({
  imports: [
    CommonModule,
    ApplicantRoutingModule,
    FormsModule,
    CKEditorModule,
    ClickOutsideModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    AngularMultiSelectModule
  ],
  declarations: [
    ApplicantComponent,
    DetailsComponent,
    ApplicantsFilterPipe,
    SortingSearchApplicantsPipe,
    TabsListComponent,
    TableHeaderComponent,
    FilterSearchComponent,
    UnreadApplicationsComponent,
    ReadApplicationsComponent,
    RejectedApplicationsComponent,
    PaginationComponent,
    FeedbackMessageComponent,
    PersonalDetailsComponent,
    SchoolDetailsComponent,
    ExtraDetailsComponent,
    AcceptMessageComponent,
    RejectMessageComponent,
    EditMessageComponent
  ],
  providers : [
    ApplicantService,
    BursaryService
  ]
})
export class ApplicantModule { }
