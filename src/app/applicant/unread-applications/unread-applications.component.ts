import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../applicant.service';

@Component({
  selector: 'app-unread-applications',
  templateUrl: './unread-applications.component.html',
  styleUrls: ['./unread-applications.component.css']
})
export class UnreadApplicationsComponent implements OnInit {
  applicationForms: any[];

  constructor(public applicantService: ApplicantService) { }

  ngOnInit() {
    if (this.applicantService.forms && this.applicantService.forms.length !== 0) {
      this.applicationForms = this.applicantService.forms;
    }
    this.applicantService.formsSub.subscribe(data => {
      this.applicationForms = data;
    });
  }

}
