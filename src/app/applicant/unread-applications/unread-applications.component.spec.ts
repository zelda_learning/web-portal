import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnreadApplicationsComponent } from './unread-applications.component';

describe('UnreadApplicationsComponent', () => {
  let component: UnreadApplicationsComponent;
  let fixture: ComponentFixture<UnreadApplicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnreadApplicationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnreadApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
