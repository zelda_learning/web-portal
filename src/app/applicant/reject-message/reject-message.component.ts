import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../applicant.service';

@Component({
  selector: 'app-reject-message',
  templateUrl: './reject-message.component.html',
  styleUrls: ['./reject-message.component.css']
})
export class RejectMessageComponent implements OnInit {

  constructor(public applicantService: ApplicantService) { }

  ngOnInit() {
  }

  sendMessage(bursaryApplication, status: string) {
    console.log('Send message called');
    // todo: send message here
    this.applicantService
      .updateApplicationStatus(bursaryApplication.applicationFormId, status);
  }

}
