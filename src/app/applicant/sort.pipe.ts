import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'sortSearchApplicants'
})

export class SortingSearchApplicantsPipe implements PipeTransform {

  transform(applicants: any[], sortText: string, searchText: string, searchByText: string, sortDirectionInc: boolean) {
    if (searchText !== '') {
      searchText = searchText.toLowerCase();
      if (searchByText === '') {
        applicants = applicants.filter(applicant => {
          if (typeof applicant.formA === 'undefined') {
            return false;
          } else {
            for (const attribute in applicant.formA) {
              if (applicant.formA[attribute].toLowerCase().includes(searchText)){
                return true;
              }
            }
            return false;
          }
        });
      } else {
        applicants = applicants.filter(applicant => {
          console.log(applicant);
          if (typeof applicant.formA === 'undefined') {
            return false;
          } else if (typeof applicant.formA[searchByText] === 'undefined') {
            return false;
          } else if ((applicant.formA[searchByText].toLowerCase()).includes(searchText)) {
            return true;
          } else {
            return false;
          }
        });
      }
    }

    if(!applicants || !sortText){
      return applicants;

    }else{
      return applicants.sort(function(applicant1, applicant2) {
        if(typeof applicant1.formA === 'undefined' && typeof applicant2.formA === 'undefined'){
          return 0;
        }else if(typeof applicant1.formA === 'undefined'){
          if(sortDirectionInc){
            return 1;
          }else{
            return -1;
          }
        }else if(typeof applicant2.formA === 'undefined'){
          if(sortDirectionInc){
            return -1;
          }else{
            return 1;
          }
        }

        var attribute1;
        var attribute2;
        if(sortText === 'status'){
          attribute1 = applicant1.status
          attribute2 = applicant2.status
        }else{
          attribute1 = applicant1.formA[sortText];
          attribute2 = applicant2.formA[sortText];
        }


        if(typeof attribute1 === 'undefined' && typeof attribute2 === 'undefined'){
          return 0;
        }else if(typeof attribute1 === 'undefined'){
          if(sortDirectionInc){
            return 1;
          }else{
            return -1;
          }
        }else if(typeof attribute2 === 'undefined'){
          if(sortDirectionInc){
            return -1;
          }else{
            return 1;
          }
        }

        if(!isNaN(attribute1) && !isNaN(attribute2)){
          attribute1 = parseInt(attribute1);
          attribute2 = parseInt(attribute2);
        }else{
          attribute1 = attribute1.toLowerCase();
          attribute2 = attribute2.toLowerCase();
        }
        if(attribute1 < attribute2){
          if(sortDirectionInc){
            return -1;
          }else{
            return 1;
          }
        };
        if(attribute1 > attribute2){
          if(sortDirectionInc){
            return 1;
          }else{
            return -1;
          }
        };
        return 0;
      });
    }
  }
}
