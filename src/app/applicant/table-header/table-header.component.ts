import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../applicant.service';

@Component({
  selector: 'app-table-header',
  templateUrl: './table-header.component.html',
  styleUrls: ['./table-header.component.css']
})
export class TableHeaderComponent implements OnInit {

  constructor(
    public applicantService: ApplicantService,
  ) { }

  ngOnInit() {
  }

}
