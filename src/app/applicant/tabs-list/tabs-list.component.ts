import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../applicant.service';

@Component({
  selector: 'app-tabs-list',
  templateUrl: './tabs-list.component.html',
  styleUrls: ['./tabs-list.component.css']
})
export class TabsListComponent implements OnInit {

  applicationForms: any[];

  constructor(public applicantService: ApplicantService) { }

  ngOnInit() {
    if (this.applicantService.forms && this.applicantService.forms.length !== 0) {
      this.applicationForms = this.applicantService.forms;
    }
    this.applicantService.formsSub.subscribe(data => {
      this.applicationForms = data;
    });
  }



}
