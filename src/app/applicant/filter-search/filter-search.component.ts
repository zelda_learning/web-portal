import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../applicant.service';

@Component({
  selector: 'app-filter-search',
  templateUrl: './filter-search.component.html',
  styleUrls: ['./filter-search.component.css']
})
export class FilterSearchComponent implements OnInit {

  constructor(public applicantService: ApplicantService) { }

  ngOnInit() {
  }

}
