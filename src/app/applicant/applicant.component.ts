import { Component, OnInit } from '@angular/core';
import {Applicant} from '../models/Applicant';
import {ApplicantService} from './applicant.service';
import {Bursary} from '../models/Bursary';
import {BursaryService} from '../bursary/bursary.service';
import {ClientAdmin} from '../models/ClientAdmin';
import {AuthService} from '../core/services/auth.service';
import {flatMap, map, switchMap, take, filter, bufferToggle} from 'rxjs/operators';
import {from, of, combineLatest } from 'rxjs';
import { ngxCsv } from 'ngx-csv/ngx-csv';


@Component({
  selector: 'app-applicant',
  templateUrl: './applicant.component.html',
  styleUrls: ['./applicant.component.css']
})
export class ApplicantComponent implements OnInit {

    csvData = [];
    formsLength = 0;
    ready = false;
    rejectMessageModalOpen: boolean = false;
   previewModalOpen: boolean = false;

    feedbackMessageEditModalOpen: boolean = false; // edit message modal
    feedbackMessageModalTab: string = 'accept';
    acceptMessage: string = '';
    rejectMessage: string = '';
    clientAdmin: ClientAdmin;

    formIds: any[] = [];
   cbaFormIds: any[] = [];
    forms: any[] = [];

    cbaForms: any[] = [];


    // selected applicant to be displayed
    selectedPageTab: string = 'All'; // sets which tab is shown in modal
    searchDetailsOption: string = '';
    searchInputValues: string = '';
    editOpen: boolean = false;
    dateValue;


    selectedBursaryForm = {
      formA: {},
      formB: {},
      formC: {},
    };

    selectedBursary: Bursary = {
      id: '',
      clientId: '',
      title: '',
      bursaryUrl: '',
      field: [],
      applicableFields: [],
      subtitle: '',
      applicationProcess: '',
      supportProvided: '',
      requirements: '',
      closingDate: new Date(),
      sections: 0,
      internalApplicationForm: true,

    };

    allCounter: number;
    flaggedCounter: number;
    shortlistedCounter: number;
    rejectedCounter: number ;


    bursaries: Bursary[] = [];
    objectKeys = Object.keys;

    i = 0;

    constructor(private bursaryService: BursaryService,
                private applicantService: ApplicantService,
                private authService: AuthService) {    }


    ngOnInit() {


      this.getBursaries();

      console.log(this.applicantService.previewModalOpen);

      // const firstBursary = this.bursaryService.getBursaries().pipe(
      //   map(data => {
      //     if (data && data[0]) {
      //       console.log(' done fetching bursaries: from init ', data);
      //       this.bursaries = data;
      //     }
      //     return data[0];
      //   }),
      //   take(1)
      // ).subscribe(data => {
      //   this.fetchApplicants(data.id);
      // });
    }


  getBursaries() {
    this.bursaryService.getBursaries().pipe(
      map(data => {

        const clientId = this.bursaryService.getClientId();

        if ( clientId === 'ZELDA') {
          data.push({
            id: 'CBA',
            clientId: 'ZELDA',
            title: 'Central Bursary Application',
            bursaryUrl: 'zelda.org.za',
            field: { All: true },
            applicableFields: [ 'All' ],
            subtitle: '',
            applicationProcess: '',
            supportProvided: '',
            requirements: '',
            closingDate: new Date(),
            openingDate: new Date(),
            sections: 0,
            internalApplicationForm: true,
          });
        }

        console.log('data', data );
        if (data && data[0]) {
          // console.log(' done fetching bursaries: header ', data);
          this.bursaries = data;
        }

        this.applicantService.setSelectedBursary(data[0]);
      }),
      take(1)
    ).subscribe();
  }


    fetchApplicants(id: string ) {// TODO: Memory leaks, redo this method
      console.log('this bursary..', id);
      console.log(this.bursaries);

      let formNumber = 0;
      for (let i = 0; i < this.bursaries.length; i++) {
        if (this.bursaries[i].id = id) {
          this.selectedBursary = this.bursaries[i];
        }
      }

      this.bursaryService.getBursaryForm(id, 'ApplicationFormA').pipe(
        map(formA => formA ? formA.formValues : {}),
        take(1)
      ).subscribe(formA => {
        this.selectedBursaryForm.formA = formA;

      });

      this.bursaryService.getBursaryForm(id, 'ApplicationFormB').pipe(
        map(formB => formB ? formB.formValues : {}),
        take(1)
      ).subscribe(formB => {
        this.selectedBursaryForm.formB = formB;
      });

      this.bursaryService.getBursaryForm(id, 'ApplicationFormC').pipe(
        map(formC => formC ? formC.formValues : {}),
        take(1)
      ).subscribe(formC => {
        this.selectedBursaryForm.formC = formC;
      });

      this.authService.getClientAdmin().pipe(
        map(clientAdmin => {
          console.log(clientAdmin);
          return clientAdmin;
        }),
        switchMap((client: any) => {
          console.log(client);
         return this.bursaryService.getBursaryMetadata(client.clientId, id);
        })
      ).subscribe(metaData => {
        this.acceptMessage = metaData.acceptMessage;
        this.rejectMessage = metaData.rejectMessage;
      });


      this.applicantService
        .getApplicationForms('CBA')
        .pipe(
          map(forms => {
              this.formsLength = forms.length;
              // console.log('cba forms', forms);


            this.i = 0;
            this.shortlistedCounter = 0;
            this.rejectedCounter = 0;
            this.flaggedCounter = 0;
            this.allCounter = forms.length;
            return from(forms).pipe(
              map(form => {
                switch (form.clientStatus) {
                  case 'Shortlisted':
                    this.shortlistedCounter += 1;
                    break;
                  case 'Rejected':
                    this.rejectedCounter += 1;
                    form.flagged = false;
                    break;
                  default:
                    break;
                }

                if (form.flagged === true) {
                  this.flaggedCounter += 1;
                }

                return form;
              })
            );
            }
          ),
          flatMap(form => {
            return form;
          }),
          map(form => {
            return form;
          }),
          filter(form => {
            // console.log(form.applicationFormId);
            if (this.formIds.indexOf(form.applicationFormId) < 0) {
             // console.log(form);
              this.formIds.push(form.applicationFormId);
              this.forms.push(form);
              // formNumber++;
              return true;
            } else {
              // formNumber++;
              if (this.forms[this.formIds.indexOf(form.applicationFormId)] === form) {
                return false;
              } else {
                this.updateObject(this.forms[this.formIds.indexOf(form.applicationFormId)], form);
                return false;
              }
            }
          }),
          map((form: any) => {
            return combineLatest(
              this.applicantService.getUserApplicationForm(form.applicationFormId, 'UserApplicationFormA')
                .pipe(map(formA => {
                  if (formA) {

                   // console.log('formA', formA);
                    if ( formA['address'] ) {
                      const address = formA['address'].split('$');
                      const newAddress = address[0] + ' ,' + address[1] + ' ,' + address[2] + ' ,' + address[3];
                      formA['address'] = newAddress;
                    }


                    return formA;
                  } else {return {}; }

                })),
              this.applicantService.getUserApplicationForm(form.applicationFormId, 'UserApplicationFormB')
                .pipe(map(formB => formB ? formB : {})),
              this.applicantService.getUserApplicationForm(form.applicationFormId, 'UserApplicationFormC')
                .pipe(map(formC => formC ? formC : {})),
              of(form)
            ).pipe(
              map(allUserForms => allUserForms),
              take(1)
            );
          }),
          flatMap(userForm => userForm)
        ).subscribe(form => {
        formNumber++;
        // console.log(form[3]);
        this.forms[this.formIds.indexOf(form[3].applicationFormId)].formA = form[0];
        this.forms[this.formIds.indexOf(form[3].applicationFormId)].formB = form[1];
        this.forms[this.formIds.indexOf(form[3].applicationFormId)].formC = form[2];

        const dataObject = { ...form[0], ...form[1], ...form[2]};

        // todo: school address reformatting
        // todo: check if this is better with a pdf

        if (Object.keys(dataObject).length > 0) {
          this.csvData.push(dataObject);
        }


        if (formNumber === this.formsLength) {
          this.ready = true;
          console.log(this.csvData);
        }

        });

    }












    // todo: fetch bursary fields from database

    // triggered whenever a tab is changed in the modal
    changePageTab(tab: string) { this.selectedPageTab = tab; }


    onSearchKey(event: KeyboardEvent) {
      this.searchInputValues = (<HTMLInputElement>event.target).value;
      console.log(this.searchInputValues);
    }





    changeSearchDetailsOption(e) {
      this.searchDetailsOption = e;
    }





      // if (status === 'shortlist') {
      //   const shortListMessage = this.acceptMessage;
      //   this.selectedApplicant.acceptanceMessage = shortListMessage.replace('_name_', 'Kho');
      //   this.previewModalOpen = false;
      //   this.acceptMessageModalOpen = !this.acceptMessageModalOpen;
      //   return;
      //
      // } else if (status === 'reject') {
      //   const rejectionMessage = this.rejectMessage;
      //   // const shortlistMessageWithUserName = shortListMessage.replace('_name_', bursaryApplication.formA.name );
      //   this.selectedApplicant.rejectionMessage = rejectionMessage.replace('_name_', 'Kho');
      //   this.previewModalOpen = false;
      //   this.rejectMessageModalOpen = !this.rejectMessageModalOpen;
      //   return;
      // }
      // this.applicantService
      //   .updateApplicationStatus(bursaryApplication.applicationFormId, status);







    rejectMessageModalToggle(bursary: Bursary) {

      if (bursary) {

        this.selectedBursary = bursary;
        console.log('accept Message modal called with bursary : ');

      }
      this.rejectMessageModalOpen = !this.rejectMessageModalOpen;
      this.previewModalOpen = !this.previewModalOpen;
    }




    changeFeedbackMessageTab( tab: string ) { this.feedbackMessageModalTab = tab; }


    feedbackMessageEditModalToggle() {
      console.clear();
      console.log('toggle feedback Modal ', this.selectedBursary);
      this.feedbackMessageEditModalOpen = !this.feedbackMessageEditModalOpen;
    }


    updateAcceptAndRejectMessages() {
      console.clear();
      console.log(this.acceptMessage, this.rejectMessage);
      this.bursaryService
        .updateBursaryMetaData(this.clientAdmin.clientId, this.selectedBursary.id,
          { acceptMessage: this.acceptMessage, rejectMessage: this.rejectMessage});
    }

    toggleEditIconDisplay(id, tabTitle) {
      const e = document.getElementById(id);
      if (tabTitle === 'Rejected' || tabTitle === 'Shortlisted') {
        e.style.display = 'block';
      } else {
        e.style.display = 'none';
      }
    }

    changeDefaultOpenMessageEditModal() {
      if (this.selectedPageTab === 'Rejected') {
        this.feedbackMessageModalTab = 'reject';
      } else {
        this.feedbackMessageModalTab = 'accept';
      }
    }

    updateObject(current, updated) {
      Object.keys(updated).forEach(function (key) {
        current[key] = updated[key];
      });
      return updated;
    }





    toggleEditOpen() {
      this.editOpen = !(this.editOpen);
    }

    onDateSelect(dateObject) {

      // Does nothing for the moment.

      // const year = dateObject.getFullYear();
      //
      // let month = dateObject.getMonth() + 1;
      // if (month.toString().length === 1) {
      //   month = '0' + month;
      // }
      // console.log(this.selectedBursary.closingDate)
      // console.log(month);
      // console.log(year);
      // console.log(this.dateValue);
    }

    download() {

      const options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        title: 'CBA Applicants Data',
        useBom: true,
        noDownload: false,
        nullToEmptyString: true,
        keys: []
      };

      new ngxCsv(this.applicantService.csvData, 'CBA Applicants Data' , options);

    }

    saveNewDate() {
      this.selectedBursary.closingDate = this.dateValue;
    }
}
