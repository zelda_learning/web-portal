import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../applicant.service';

@Component({
  selector: 'app-read-applications',
  templateUrl: './read-applications.component.html',
  styleUrls: ['./read-applications.component.css']
})
export class ReadApplicationsComponent implements OnInit {

  applicationForms: any[];

  constructor(public applicantService: ApplicantService) { }

  ngOnInit() {

    if (this.applicantService.forms && this.applicantService.forms.length !== 0) {
      this.applicationForms = this.applicantService.forms;
    }
    this.applicantService.formsSub.subscribe(data => {
      this.applicationForms = data;
    });
  }

}
