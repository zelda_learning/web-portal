import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'statusFilter'
})
export class ApplicantsFilterPipe implements PipeTransform {
  transform(items: any[], filterText: string): any[] {
    if (!items) { return []; }

    if (!filterText) { return items; }
    if (filterText === 'All') { return items; }

    return items.filter( item => {
      if (item.status === filterText) {
        return item;
      } else if (filterText === 'Flagged') {

        if (item.flagged === true ) { return item; }

      }
    });
  }
}
