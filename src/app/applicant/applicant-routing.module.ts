import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ApplicantComponent } from './applicant.component';
import { DetailsComponent } from './details/details.component';

const routes: Routes = [
  {path : "" , component : ApplicantComponent},
  {path : ":id", component : DetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ApplicantRoutingModule { }
