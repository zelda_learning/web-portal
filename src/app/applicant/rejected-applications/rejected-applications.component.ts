import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../applicant.service';

@Component({
  selector: 'app-rejected-applications',
  templateUrl: './rejected-applications.component.html',
  styleUrls: ['./rejected-applications.component.css']
})
export class RejectedApplicationsComponent implements OnInit {

  applicationForms: any[];

  constructor(
    public applicantsService: ApplicantService,
  ) { }

  ngOnInit() {

    if (this.applicantsService.forms && this.applicantsService.forms.length !== 0) {
      this.applicationForms = this.applicantsService.forms;
    }
    this.applicantsService.formsSub.subscribe(data => {
      this.applicationForms = data;
    });
  }

}
