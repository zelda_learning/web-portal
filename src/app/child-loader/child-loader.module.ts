import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChildLoaderRoutingModule } from './child-loader-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ChildLoaderRoutingModule
  ],
  declarations: []
})
export class ChildLoaderModule { }
