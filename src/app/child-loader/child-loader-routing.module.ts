import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path : 'dashboard', loadChildren : '../dashboard/dashboard.module#DashboardModule'},
  {path : '#', loadChildren : '../dashboard/dashboard.module#DashboardModule'},
  {path : 'bursary', loadChildren : '../bursary/bursary.module#BursaryModule'},
  {path : 'applicants', loadChildren : '../applicant/applicant.module#ApplicantModule'},
  {path : 'profile', loadChildren : '../profile/profile.module#ProfileModule'},
  {path : 'messaging', loadChildren : '../messaging/messaging.module#MessagingModule'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChildLoaderRoutingModule { }
