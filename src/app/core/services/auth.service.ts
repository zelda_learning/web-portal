import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { map, switchMap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { ClientAdmin } from '../../models/ClientAdmin';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  clientAdminDocument: AngularFirestoreDocument<ClientAdmin>;
  clientAdmin: Observable<ClientAdmin>;
  clientId: string;
  user: any;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) {
    // Get auth data, then get Firestore clientAdmin document // null
    this.clientAdmin = this.afAuth.authState.pipe(
      switchMap((user: any) => {
        if (user) {
          console.log('current logged in user ', user.email );

          this.user = user;

          // return observable of client admin data in the database
          return this.afs.doc<ClientAdmin>(`ClientsAdmins/${user.uid}`).valueChanges();
        } else {
          // return observable of null if the user object is not yet in the database
          return of(user);
        }
      })
    );
  }


  login(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((userData) => {
        resolve(userData);
      })
      .catch((err) => {
        reject(err);
      });
    });
  }

  register(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((userData) => {
        resolve(userData);
      })
      .catch((err) => {
        reject(err);
      });
    });
  }

  getAuth() {
    return this.afAuth.authState.pipe(map((auth) => auth));
  }

getClientAdmin() {
    return this.clientAdmin;
  }

  logout() {
    this.afAuth.auth.signOut();
  }

  sendPasswordResetEmail() {
    return this.afAuth.auth.sendPasswordResetEmail(this.user.email)
      .then((success) => 'A password reset email has been sent with further instructions')
      .catch((error) => 'Oops, something went wrong please try again later.');

  }

  addClientIdOnRegistration(userData, clientId) {
    this.clientAdminDocument = this.afs.doc(`ClientsAdmins/${userData.user.uid}`);
    const data = {
      clientAdminId: userData.user.uid,
      email: userData.user.email,
      clientId: clientId
    };

    // @ts-ignore
    return this.clientAdminDocument.set(data, {merge: true});
  }
}
