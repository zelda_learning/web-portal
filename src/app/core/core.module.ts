import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { environment } from '../../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { SideNavbarComponent } from './side-navbar/side-navbar.component';


import { PanelLayoutComponent} from './layout/panel-layout.component';

import { ClientService } from './services/client.service';
import {AngularFirestore} from '@angular/fire/firestore';
import { BursaryService} from '../bursary/bursary.service';
import {ApplicantService} from '../applicant/applicant.service';

import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule, MatTooltipModule
} from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    AngularFireModule.initializeApp(environment.firebase, 'zeldav1-dev'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    NotFoundComponent,
    TopNavbarComponent,
    SideNavbarComponent,
    PanelLayoutComponent
  ],
  providers: [
    ClientService,
    AngularFireStorage,
    BursaryService,
    ApplicantService
  ]
})
export class CoreModule {}
