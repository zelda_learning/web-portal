import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ClientService } from '../services/client.service';
import { switchMap } from 'rxjs/operators';
import {BursaryService} from '../../bursary/bursary.service';
import {Bursary} from '../../models/Bursary';
import {ApplicantService} from '../../applicant/applicant.service';


@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.css']
})
export class SideNavbarComponent implements OnInit {
  accOneSelected: boolean = true;
  sideNavMinimised: boolean = false;
  clientLogoUrl: string = '';
  bursaries: Bursary[] = [];

  @Output() toggleSideNavMinimised = new EventEmitter<boolean>();

  constructor(
    private bursaryService: BursaryService,
    private applicantService: ApplicantService,
    private clientService: ClientService,
    private authService: AuthService
  ) { }

  ngOnInit() {

    this.getBursaries();
    this.getClientLogo();

  }

    // toggles bursaries accordion
    onAccOneClick() { this.accOneSelected = !this.accOneSelected; }
    // toggles side nav size when hamburger is clicked
    onHamClick() {
      this.sideNavMinimised = !this.sideNavMinimised;
      this.toggleSideNavMinimised.next(this.sideNavMinimised);
    }

    getClientLogo() {
      this.authService.getClientAdmin().pipe(
        switchMap(admin => {
          return this.clientService.getClient(admin.clientId);
        })
      ).subscribe(client => {
        this.clientLogoUrl = client.photo;
      });

    }

    getBursaries() {
      this.bursaryService
        .getBursaries()
        .subscribe(data => {

          const clientId = this.bursaryService.getClientId();

          if ( clientId === 'ZELDA') {
            data.push({
              id: 'CBA',
              clientId: 'ZELDA',
              title: 'Central Bursary Application',
              bursaryUrl: 'zelda.org.za',
              field: { All: true },
              applicableFields: [ 'All' ],
              subtitle: '',
              applicationProcess: '',
              supportProvided: '',
              requirements: '',
              closingDate: new Date(),
              openingDate: new Date(),
              sections: 0,
              internalApplicationForm: true,
            });
          }
          console.log('data', data );
          if (data && data[0]) {
           // console.log(' done fetching bursaries: from side nav ', data);
            this.bursaries = data;
            this.applicantService.setSelectedBursary(data[0]);
          }
        });
    }
}
