import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email: string;
  password: string;
  clientId: string;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    // if user is already logged in navigate away
    this.authService.getAuth().subscribe((auth) => {
      if (auth) {
        this.router.navigate(['/']);
      }
    });
  }

  // Sign up button clicked
  onSubmit() {

    // todo: first check if a client with provided clientId exists
    // todo: add first name and last name fields


    this.authService.register(this.email, this.password)
    .then((res) => {

      // add current user to client admins document
      this.authService.addClientIdOnRegistration(res, this.clientId)
        .then(() => {

          this.router.navigate(['/']);

        })
        .catch((error) => {

          console.log('addClientOnRegistration error: ', error.message);
        });



    })
    .catch((err) => {
      console.log('Register on submit error: ', err.message);
    });
  }
}
