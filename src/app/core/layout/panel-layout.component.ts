import { Component, OnInit} from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';

@Component({
  selector: 'app-panel-layout',
  templateUrl: './panel-layout.component.html',
  styleUrls: ['./panel-layout.component.css']
})
export class PanelLayoutComponent implements OnInit {

  sideNavMinimised: boolean;
  currentRoute: string;

  constructor(private router: Router) { }

  ngOnInit() {
      this.checkRoute();
  }


  checkRoute() {
    // change the title on the tab depending on route
    this.router.events.subscribe((e: any) => {

      // check final URL and change navigation text
      if (e instanceof NavigationEnd) {
        switch (e.url) {
          case '/bursary/new':
            this.currentRoute = 'Create new bursary';
            return;
          case '/bursary/:id':
            this.currentRoute = 'Manage Bursary';
            return;
          case '/applicants':
            this.currentRoute = 'Manage Bursary Applications';
            return;

          default:
            this.currentRoute = '';
            return;
        }
      }

    });
  }
  sideNavToggle(something) { this.sideNavMinimised = something; }
}
