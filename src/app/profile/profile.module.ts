import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileComponent } from './profile.component';

import {ClientAdminService} from './client-admin.service';

import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    FormsModule,
    CKEditorModule
  ],
  declarations: [ProfileComponent],
  providers : [
    ClientAdminService
  ]
})
export   class ProfileModule { }
