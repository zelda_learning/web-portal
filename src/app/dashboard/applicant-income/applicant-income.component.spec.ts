import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantIncomeComponent } from './applicant-income.component';

describe('ApplicantIncomeComponent', () => {
  let component: ApplicantIncomeComponent;
  let fixture: ComponentFixture<ApplicantIncomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantIncomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantIncomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
