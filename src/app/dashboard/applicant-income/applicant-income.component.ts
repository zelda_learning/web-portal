import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../../applicant/applicant.service';

@Component({
  selector: 'app-applicant-income',
  templateUrl: './applicant-income.component.html',
  styleUrls: ['./applicant-income.component.css']
})
export class ApplicantIncomeComponent implements OnInit {

  applicantIncomeOptions: object = {
    legend: { position: 'none' },
    colors: ['#37aaa4'],
    bar: {groupWidth: '95%' }
  };
  applicantIncomeColumnNames: any[] = [ 'Income Bracket', 'Number of Students'];
  applicantIncomeData: any[] = [
    ['<R120 000', 0 ],
    ['R120 000 - R300 000', 0],
    ['R300 000 - R600 000', 0],
    ['>R600 000', 0]
  ];
  ready = false;
  applicationForms: any[];

  constructor(public applicants: ApplicantService) { }

  ngOnInit() {
    if (this.applicants.forms && this.applicants.forms.length !== 0) {
      this.applicationForms = this.applicants.forms;
      this.updateIncomeData();
    }
    this.applicants.formsSub.subscribe(data =>{
      this.applicationForms = data;
      this.updateIncomeData();
    });
  }

  updateIncomeData () {

    this.applicationForms.forEach(form => {

      // console.log('income', form.formA.combinedYearlyIncome );
      switch (form.formA.combinedYearlyIncome ) {

        case '<R120 000':
          this.applicantIncomeData[0][1] ++  ;
          break;

        case 'R120 000 - R300 000':

          this.applicantIncomeData[1][1] ++ ;

          break;
        case 'R300 000 - R600 000':
          this.applicantIncomeData[2][1] ++ ;

          break;

        case '>R600 000':
          this.applicantIncomeData[3][1] ++;
          break;

        default: return;
      }
    });

    this.ready = true;
  }

}
