import { Component, OnInit , ViewChild} from '@angular/core';
import {Applicant} from '../models/Applicant';
import {ApplicantService} from '../applicant/applicant.service';
import {Bursary} from '../models/Bursary';
import {BursaryService} from '../bursary/bursary.service';
import {ClientAdmin} from '../models/ClientAdmin';
import {AuthService} from '../core/services/auth.service';
import {flatMap, map, switchMap, take, filter, bufferToggle} from 'rxjs/operators';
import {from, of, combineLatest } from 'rxjs';
import {ClientService} from '../core/services/client.service';
import {promise} from 'selenium-webdriver';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {


  formsLength = 0;
  bursaries: Bursary[] = [];

  selectedBursaryForm = {
    formA: {},
    formB: {},
    formC: {},
  };

  formIds: any[] = [];
  forms: any[] = [];
  i = 0;


  ready = false;

  constructor(
    private bursaryService: BursaryService,
    private applicantService: ApplicantService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    // this.applicantService.initAppForms();
    // console.log(this.applicantService);
  }




}
