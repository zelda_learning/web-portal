import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { GoogleChartsModule } from 'angular-google-charts';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { ApplicantService } from '../applicant/applicant.service';
import {BursaryService} from '../bursary/bursary.service';
import { DashboardHeaderComponent } from './dashboard-header/dashboard-header.component';
import { ApplicantsDistributionComponent } from './applicants-distribution/applicants-distribution.component';
import { ApplicantsDemographicComponent } from './applicants-demographic/applicants-demographic.component';
import { ApplicantMarkComponent } from './applicant-mark/applicant-mark.component';
import { ApplicantIncomeComponent } from './applicant-income/applicant-income.component';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';

import {
  MatDatepickerModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatInputModule, MatTooltipModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    GoogleChartsModule.forRoot('AIzaSyAcYazwXWztv7XqAPHjSNEqsUUmCR5K-lU'),
    AngularMultiSelectModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
  ],
  declarations: [DashboardComponent, DashboardHeaderComponent,
    ApplicantsDistributionComponent, ApplicantsDemographicComponent,
    ApplicantMarkComponent, ApplicantIncomeComponent],
  providers : [
    ApplicantService,
    BursaryService
  ]
})
export class DashboardModule { }
