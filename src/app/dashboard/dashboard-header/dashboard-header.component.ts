import { Component, OnInit } from '@angular/core';
import {flatMap, map, switchMap, take, filter, bufferToggle} from 'rxjs/operators';


import {ApplicantService} from '../../applicant/applicant.service';
import {BursaryService} from '../../bursary/bursary.service';
import {Bursary} from '../../models/Bursary';

@Component({
  selector: 'app-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.css']
})
export class DashboardHeaderComponent implements OnInit {

  bursaries: Bursary[] = [];
  selectedBursary: any;

  constructor(public applicantService: ApplicantService,
              private bursaryService: BursaryService,
  ) { }

  ngOnInit() {
   // console.log('selected bursary  header dashboard' , this.applicantService.selectedBursary);
   // console.log(this.selectedBursary);
    this.getBursaries();
  }

  setSelectedBursary(value: any) {
    this.bursaryService.getBursary(value.target.value).subscribe(
      data => {
        console.log(data);
        this.applicantService.setSelectedBursary(data);
      }
    );
    // console.log(value, this.applicantService.selectedBursary);
  }

  getBursaries() {
    this.bursaryService.getBursaries().pipe(
      map(data => {

        const clientId = this.bursaryService.getClientId();

        if ( clientId === 'ZELDA') {
          data.push({
            id: 'CBA',
            clientId: 'ZELDA',
            title: 'Central Bursary Application',
            bursaryUrl: 'zelda.org.za',
            field: { All: true },
            applicableFields: [ 'All' ],
            subtitle: '',
            applicationProcess: '',
            supportProvided: '',
            requirements: '',
            closingDate: new Date(),
            openingDate: new Date(),
            sections: 0,
            internalApplicationForm: true,
          });
        }

        console.log('data', data );
        if (data && data[0]) {
         // console.log(' done fetching bursaries: header ', data);
          this.bursaries = data;
        }

        this.applicantService.setSelectedBursary(data[0]);
      }),
      take(1)
    ).subscribe();
  }

}
