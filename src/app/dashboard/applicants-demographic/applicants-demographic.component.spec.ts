import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantsDemographicComponent } from './applicants-demographic.component';

describe('ApplicantsDemographicComponent', () => {
  let component: ApplicantsDemographicComponent;
  let fixture: ComponentFixture<ApplicantsDemographicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantsDemographicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantsDemographicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
