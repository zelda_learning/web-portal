import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../../applicant/applicant.service';

@Component({
  selector: 'app-applicants-demographic',
  templateUrl: './applicants-demographic.component.html',
  styleUrls: ['./applicants-demographic.component.css']
})
export class ApplicantsDemographicComponent implements OnInit {

  ready = false;
  applicantRaceData: any[] = [
    ['Black', 1],
    ['White', 1],
    ['Asian', 1],
    ['Coloured', 1],
    ['Indian', 1],
    ['Prefer not to say', 1],
    ['Other', 1]
  ];

  applicantRaceOptions: object = {
    colors: ['#E48F1C', '#FAA532', '#FBB75B', '#FCC984', '#FDDBAD', '#FDE4C1', '#FEF6EA']

  };
  applicantRaceColumnNames: any[] = [ 'Race', 'Number'];

  genderDataFemale: number  = 0 ;
  genderDataPreferNotToSay: number  = 0 ;
  genderDataMale: number  = 0 ;
  genderDataOther: number = 0;

  genderDataTotal: number  = 0 ;

  genderDataFemalePer: number  = 0 ;
  genderDataPreferNotToSayPer: number  = 0 ;
  genderDataMalePer: number  = 0 ;
  genderDataOtherPer: number  = 0 ;


  applicationForms: any[];

  constructor(public applicants: ApplicantService) { }

  ngOnInit() {
    if (this.applicants.forms && this.applicants.forms.length !== 0) {
      this.applicationForms = this.applicants.forms;
      this.updateDemographicsData();
    }
    this.applicants.formsSub.subscribe(data => {
      this.applicationForms = data;
      this.updateDemographicsData();
    });

  }

  updateDemographicsData () {

    this.applicationForms.forEach(form => {

      switch (form.formA.race) {

        case 'Black':
          this.applicantRaceData[0][1] = this.applicantRaceData[0][1] + 1;
          break;

        case 'White':
          this.applicantRaceData[1][1] = this.applicantRaceData[1][1] + 1;
          break;
        case 'Asian':
          this.applicantRaceData[2][1] = this.applicantRaceData[2][1] + 1;
          break;

        case 'Coloured':
          this.applicantRaceData[3][1] = this.applicantRaceData[3][1] + 1;
          break;

        case 'Indian':
          this.applicantRaceData[4][1] = this.applicantRaceData[4][1] + 1;
          break;

        case 'Prefer not to say':
          this.applicantRaceData[5][1] = this.applicantRaceData[5][1] + 1;
          break;

        case 'Other':
          this.applicantRaceData[6][1] = this.applicantRaceData[6][1] + 1;
          break;

        default:  break;
      }

      switch (form.formA.gender ) {

        case 'Female':

          this.genderDataFemale += 1 ;
          this.genderDataTotal = this.genderDataTotal + 1;
          break;

        case 'Prefer not to say':
          this.genderDataPreferNotToSay += 1 ;
          this.genderDataTotal = this.genderDataTotal + 1;
          break;
        case 'Male':
          this.genderDataMale += 1 ;
          this.genderDataTotal = this.genderDataTotal + 1;
          break;

        case 'Other':
          this.genderDataOther += 1 ;
          this.genderDataTotal = this.genderDataTotal + 1;
          break;

        default:  return;
      }


      this.genderDataFemalePer = Math.floor((this.genderDataFemale / this.genderDataTotal ) * 200);
      this.genderDataPreferNotToSayPer = Math.floor((this.genderDataPreferNotToSay / this.genderDataTotal ) * 200);
      this.genderDataMalePer = Math.floor((this.genderDataMale / this.genderDataTotal ) * 200);
      this.genderDataOtherPer = Math.floor((this.genderDataOther / this.genderDataTotal ) * 200);


    });

    this.ready = true;
  }

}
