import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantsDistributionComponent } from './applicants-distribution.component';

describe('ApplicantsDistributionComponent', () => {
  let component: ApplicantsDistributionComponent;
  let fixture: ComponentFixture<ApplicantsDistributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantsDistributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantsDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
