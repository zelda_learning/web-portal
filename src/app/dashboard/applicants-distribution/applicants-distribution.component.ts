import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../../applicant/applicant.service';

@Component({
  selector: 'app-applicants-distribution',
  templateUrl: './applicants-distribution.component.html',
  styleUrls: ['./applicants-distribution.component.css']
})
export class ApplicantsDistributionComponent implements OnInit {

  ready = false;
  applicantDistributionOptions: object = {
    region: 'ZA', // Africa
    resolution: 'provinces',
    displayMode: 'markers',
    colorAxis: {colors: ['#57bab5', '#125248']},
    datalessRegionColor: '#FCC984',
    backgroundColor: { stroke: '#01e107', strokeWidth : 2},
    domain: 'ZA'
  };

  applicantDistributionColumnNames: any[] = [ 'Area', 'Number of Students', 'Percentage'];
  applicantDistributionData: any[] = [];
  applicantDistributionDataSort: object  = {};
  private applicationForms: any;


  constructor(public applicants: ApplicantService) { }

  ngOnInit() {
    if (this.applicants.forms && this.applicants.forms.length !== 0) {
      this.applicationForms = this.applicants.forms;
      this.updateDistributionData();
    }
    this.applicants.formsSub.subscribe(data => {
      this.applicationForms = data;
    //  console.log('forms', data);
      this.updateDistributionData();
    });

  }

  updateDistributionData () {

    this.applicationForms.forEach(form => {
      let postalCode = '';

      if (form.formA.zipCode) {
        postalCode = form.formA.zipCode;


        if ( this.applicantDistributionDataSort.hasOwnProperty(postalCode) ) {

          this.applicantDistributionDataSort[postalCode] += 1;

        } else {

          this.applicantDistributionDataSort[postalCode] = 1;
        }

        const keys = Object.keys(this.applicantDistributionDataSort);

        let total = 0;
        for (const key of keys) {
          total = total + this.applicantDistributionDataSort[key];
        }

        this.applicantDistributionData = [];
        for (const key of keys) {
          const keyValue =  this.applicantDistributionDataSort[key];
          this.applicantDistributionData.push([key, keyValue, Math.floor(100 * keyValue / total) ]);
        }

      }

    });

    this.ready = true;
  }

}
