import { Component, OnInit } from '@angular/core';
import {ApplicantService} from '../../applicant/applicant.service';

@Component({
  selector: 'app-applicant-mark',
  templateUrl: './applicant-mark.component.html',
  styleUrls: ['./applicant-mark.component.css']
})
export class ApplicantMarkComponent implements OnInit {

  applicantAggregateMarksOptions: object = {
    legend: { position: 'none' },
    colors: ['#E48F1C'],
    bar: {groupWidth: '95%' }
  };

  applicantAggregateMarksColumnNames: any[] = [ 'Mark', 'Number of students'];

  applicantAggregateMarksData: any[] = [
     ['40', 0],
     ['50', 0],
     ['60', 0],
     ['70', 0],
     ['80', 0],
     ['90' , 0],
     ['100' , 0],
   ];

  ready = false;
  applicationForms: any[];

  constructor(public applicants: ApplicantService) {

  }

  ngOnInit() {

    if (this.applicants.forms && this.applicants.forms.length !== 0) {
      this.applicationForms = this.applicants.forms;
      this.updateMarksData();
    }
    this.applicants.formsSub.subscribe(data => {
      this.applicationForms = data;
      this.updateMarksData();
    });
  }

  updateMarksData () {

    this.applicationForms.forEach(form => {

      if (form.formB.aggregateMark) {

        const mark = form.formB.aggregateMark;

        // console.log('Mark ', mark, average, total, form.formB.subjectMarksMostRecent);

        if (mark >= 40 && mark < 50){
          this.applicantAggregateMarksData[0][1] ++;
        }

        if (mark >= 50 && mark < 60){
          this.applicantAggregateMarksData[1][1] ++ ;

        }

        if (mark >= 60 && mark < 70) {
          this.applicantAggregateMarksData[2][1] ++ ;

        }

        if (mark >= 70 && mark < 80) {
          this.applicantAggregateMarksData[3][1] ++;

        }

        if (mark >= 80 && mark < 90) {
          this.applicantAggregateMarksData[4][1] ++;

        }

        if ( mark >= 90 && mark < 100) {
          this.applicantAggregateMarksData[5][1] ++;

        }

        if ( mark >= 100 ) {
          this.applicantAggregateMarksData[6][1] ++;
        }
      }
    });

    this.ready = true;
  }


}
