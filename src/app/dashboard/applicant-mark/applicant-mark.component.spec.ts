import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicantMarkComponent } from './applicant-mark.component';

describe('ApplicantMarkComponent', () => {
  let component: ApplicantMarkComponent;
  let fixture: ComponentFixture<ApplicantMarkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicantMarkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicantMarkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
