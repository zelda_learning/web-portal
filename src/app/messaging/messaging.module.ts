import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MessagingRoutingModule } from './messaging-routing.module';
import { MessagingComponent } from './messaging.component';

import { CKEditorModule } from 'ng2-ckeditor';

@NgModule({
  imports: [
    CommonModule,
    MessagingRoutingModule,
    CKEditorModule
  ],
  declarations: [MessagingComponent]
})
export class MessagingModule { }
