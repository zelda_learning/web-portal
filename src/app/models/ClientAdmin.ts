export interface ClientAdmin {
  readList: object;
  flaggedList: object;
  shortlistedList: object;
    clientAdminId?: string;
    email?: string;
    clientId?: string;
    firstName?: string;
    lastName?: string;
}
