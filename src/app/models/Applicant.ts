export interface Applicant {
    applicationId?: string; // Id given to the bursary application
    userId?: string; // Id of user applying
    status?: string;
    clientStatus?: string;
    applicationFormId?: string;
    formA?: any;
    formB?: any;
    formC?: any;
  acceptanceMessage?: string;
  rejectionMessage?: string;
  readStatus?: string;
  flagged?: boolean;
}
