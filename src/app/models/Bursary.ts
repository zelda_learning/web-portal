export interface Bursary {
    id?: string;
    clientId?: string;
    title?: string;
    bursaryUrl?: string;
    field?: object;
    applicableFields?: string[];
    subtitle?: string;
    applicationProcess?: string;
    supportProvided?: string;
    requirements?: string;
    closingDate?: any;
    openingDate?: any;
    sections?: number;
    internalApplicationForm?: boolean;
    draft?: boolean;
    published?: boolean;

}

export interface BursaryMetaData {
  acceptMessage?: string;
  rejectMessage?: string;
  feedbackSent?: string;
}

export interface BursaryForm {
  formValues?: object;

}
