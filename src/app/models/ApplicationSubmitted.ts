export interface ApplicationSubmitted {
    applicationId?: string;
    submitted?: boolean;
    userId?: string;
    formId?: string;
}
