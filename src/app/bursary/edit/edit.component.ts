import { Component, OnInit, ViewChild } from '@angular/core';
import { Bursary } from '../../models/Bursary';
import { Router, ActivatedRoute } from '@angular/router';
import { BursaryService } from '../bursary.service';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

    bursaryId: string;
    bursary: Bursary = {
      id: '',
      clientId: '',
      title: '',
      bursaryUrl: '',
      field: [],
      applicableFields: [],
      subtitle: '',
      applicationProcess: '',
      supportProvided: '',
      requirements: '',
      closingDate: new Date(),
      sections: 0,
      internalApplicationForm: true
    };

    // career main field options
    fieldsOptions = [
      { 'id': 1, 'itemName': 'Agriculture, Food and Natural Resources'},
      { 'id': 2, 'itemName': 'Artisanal Trades' },
      { 'id': 3, 'itemName': 'Commerce' },
      { 'id': 4, 'itemName': 'Engineering and the Built Environment' },
      { 'id': 5, 'itemName': 'Health Sciences'},
      { 'id': 6, 'itemName': 'Hospitality, Tourism and Transport' },
      { 'id': 7, 'itemName': 'Humanities' },
      { 'id': 8, 'itemName': 'Law' },
      { 'id': 9, 'itemName': 'Science' }
  ];

    // field options sub fields
    applicableFieldsOptions = [
      { 'id': 1, 'itemName': 'Accounting', 'parentID' : 3 },
      { 'id': 2, 'itemName': 'Actuarial Science', 'parentID' : 3 },
      { 'id': 3, 'itemName': 'Business, Management and Administration', 'parentID' : 3 },
      { 'id': 4, 'itemName': 'Business Science', 'parentID' : 3 },
      { 'id': 5, 'itemName': 'Economics' , 'parentID' : 3},
      { 'id': 6, 'itemName': 'Finance' , 'parentID' : 3},
      { 'id': 7, 'itemName': 'Information Systems', 'parentID' : 3 },
      { 'id': 8, 'itemName': 'Marketing' , 'parentID' : 3},
      { 'id': 9, 'itemName': 'PPE - Politics, Philosophy and Economics', 'parentID' : 3 },
      { 'id': 10, 'itemName': 'Architecture', 'parentID' : 4 },
      { 'id': 11, 'itemName': 'Chemical Engineering', 'parentID' : 4 },
      { 'id': 12, 'itemName': 'Civil Engineering', 'parentID' : 4 },
      { 'id': 13, 'itemName': 'Construction Studies', 'parentID' : 4 },
      { 'id': 14, 'itemName': 'Computer Engineering', 'parentID' : 4 },
      { 'id': 15, 'itemName': 'Electrical Engineering', 'parentID' : 4 },
      { 'id': 16, 'itemName': 'Geomatics', 'parentID' : 4 },
      { 'id': 17, 'itemName': 'Mechanical Engineering', 'parentID' : 4 },
      { 'id': 18, 'itemName': 'Medicine', 'parentID' : 5 },
      { 'id': 19, 'itemName': 'Pharmaceutical Science', 'parentID' : 5 },
      { 'id': 20, 'itemName': 'Nursing', 'parentID' : 5 },
      { 'id': 21, 'itemName': 'Arts and Culture', 'parentID' : 7 },
      { 'id': 22, 'itemName': 'Education', 'parentID' : 7 },
      { 'id': 23, 'itemName': 'History', 'parentID' : 7 },
      { 'id': 24, 'itemName': 'Languages', 'parentID' : 7},
      { 'id': 25, 'itemName': 'Literature', 'parentID' : 7 },
      { 'id': 26, 'itemName': 'Media Studies', 'parentID' : 7 },
      { 'id': 27, 'itemName': 'Philosophy', 'parentID' : 7 },
      { 'id': 28, 'itemName': 'Politics', 'parentID' : 7 },
      { 'id': 29, 'itemName': 'Psychology', 'parentID' : 7 },
      { 'id': 30, 'itemName': 'Social Sciences', 'parentID' : 7 },
      { 'id': 31, 'itemName': 'Applied Mathematics and Mathematics', 'parentID' : 9 },
      { 'id': 32, 'itemName': 'Astronomy and Astrophysics', 'parentID' : 9 },
      { 'id': 33, 'itemName': 'Biology' , 'parentID' : 9},
      { 'id': 34, 'itemName': 'Chemistry', 'parentID' : 9 },
      { 'id': 35, 'itemName': 'Computer Science', 'parentID' : 9 },
      { 'id': 36, 'itemName': 'Environmental Sciences' , 'parentID' : 9},
      { 'id': 37, 'itemName': 'Geology' , 'parentID' : 9},
      { 'id': 38, 'itemName': 'Information Technology', 'parentID' : 9 },
      { 'id': 39, 'itemName': 'Physics' , 'parentID' : 9},
      { 'id': 40, 'itemName': 'Statistics', 'parentID' : 9 },
      { 'id': 41, 'itemName': 'Veterinary Science', 'parentID' : 9 },
    ];
    selectedFieldOptions = [];
    selectedApplicatbleFieldOptions = [];
    fieldDropdownSettings = {};
    courseDropdownSettings = {};

    alertMessage: string = ''; // give updates to user

    clientId: string;

    description: string;

    editState: boolean;

    dropdownSettings = {};


  constructor(private bursaryService: BursaryService,
    private route: ActivatedRoute,
    private authService: AuthService) { }

  ngOnInit() {


    this.authService.clientAdmin.subscribe(data => {
      if (data) {
        this.clientId = data.clientId;
        console.log(this.clientId);
      }
    });

    // get bursary details of currently selected bursary
    this.bursaryId =  this.route.snapshot.params['id'];
    this.bursaryService.getBursary(this.bursaryId).subscribe(data => {
      if (data != null) {
        console.log('Success fetching bursary to edit ');

        this.bursary = data;
        this.description = this.bursary.subtitle; // set local description for bursary
        // @ts-ignore
        this.bursary.closingDate = data.closingDate.toDate(); // change date format

        const keys = Object.keys(data.field);
        // set field options using field options from database
        keys.forEach(databaseField => {

          this.fieldsOptions.forEach((localField) => {

            if (localField.itemName === databaseField) {
              this.selectedFieldOptions.push(localField);
            }
          });

        });
        // populate the data after loop finishes
        this.bursary.field = this.selectedFieldOptions;

        // set applicableField options using applicable field option from database
        data.applicableFields.forEach(databaseApplicableField => {

          this.applicableFieldsOptions.forEach((localApplicableField) => {

            if (localApplicableField.itemName === databaseApplicableField) {
              this.selectedApplicatbleFieldOptions.push(localApplicableField);
            }
          });

        });
        // populate the data after loop finishes
        this.bursary.applicableFields = this.selectedApplicatbleFieldOptions;

      } else {
        console.log('Error loading bursary');
      }
    });





    // field options sub fields
    this.applicableFieldsOptions = [
      { 'id': 1, 'itemName': 'Accounting', 'parentID' : 3 },
      { 'id': 2, 'itemName': 'Actuarial Science', 'parentID' : 3 },
      { 'id': 3, 'itemName': 'Business, Management and Administration', 'parentID' : 3 },
      { 'id': 4, 'itemName': 'Business Science', 'parentID' : 3 },
      { 'id': 5, 'itemName': 'Economics' , 'parentID' : 3},
      { 'id': 6, 'itemName': 'Finance' , 'parentID' : 3},
      { 'id': 7, 'itemName': 'Information Systems', 'parentID' : 3 },
      { 'id': 8, 'itemName': 'Marketing' , 'parentID' : 3},
      { 'id': 9, 'itemName': 'PPE - Politics, Philosophy and Economics', 'parentID' : 3 },
      { 'id': 10, 'itemName': 'Architecture', 'parentID' : 4 },
      { 'id': 11, 'itemName': 'Chemical Engineering', 'parentID' : 4 },
      { 'id': 12, 'itemName': 'Civil Engineering', 'parentID' : 4 },
      { 'id': 13, 'itemName': 'Construction Studies', 'parentID' : 4 },
      { 'id': 14, 'itemName': 'Computer Engineering', 'parentID' : 4 },
      { 'id': 15, 'itemName': 'Electrical Engineering', 'parentID' : 4 },
      { 'id': 16, 'itemName': 'Geomatics', 'parentID' : 4 },
      { 'id': 17, 'itemName': 'Mechanical Engineering', 'parentID' : 4 },
      { 'id': 18, 'itemName': 'Medicine', 'parentID' : 5 },
      { 'id': 19, 'itemName': 'Pharmaceutical Science', 'parentID' : 5 },
      { 'id': 20, 'itemName': 'Nursing', 'parentID' : 5 },
      { 'id': 21, 'itemName': 'Arts and Culture', 'parentID' : 7 },
      { 'id': 22, 'itemName': 'Education', 'parentID' : 7 },
      { 'id': 23, 'itemName': 'History', 'parentID' : 7 },
      { 'id': 24, 'itemName': 'Languages', 'parentID' : 7},
      { 'id': 25, 'itemName': 'Literature', 'parentID' : 7 },
      { 'id': 26, 'itemName': 'Media Studies', 'parentID' : 7 },
      { 'id': 27, 'itemName': 'Philosophy', 'parentID' : 7 },
      { 'id': 28, 'itemName': 'Politics', 'parentID' : 7 },
      { 'id': 29, 'itemName': 'Psychology', 'parentID' : 7 },
      { 'id': 30, 'itemName': 'Social Sciences', 'parentID' : 7 },
      { 'id': 31, 'itemName': 'Applied Mathematics and Mathematics', 'parentID' : 9 },
      { 'id': 32, 'itemName': 'Astronomy and Astrophysics', 'parentID' : 9 },
      { 'id': 33, 'itemName': 'Biology' , 'parentID' : 9},
      { 'id': 34, 'itemName': 'Chemistry', 'parentID' : 9 },
      { 'id': 35, 'itemName': 'Computer Science', 'parentID' : 9 },
      { 'id': 36, 'itemName': 'Environmental Sciences' , 'parentID' : 9},
      { 'id': 37, 'itemName': 'Geology' , 'parentID' : 9},
      { 'id': 38, 'itemName': 'Information Technology', 'parentID' : 9 },
      { 'id': 39, 'itemName': 'Physics' , 'parentID' : 9},
      { 'id': 40, 'itemName': 'Statistics', 'parentID' : 9 },
      { 'id': 41, 'itemName': 'Veterinary Science', 'parentID' : 9 },
    ];

    // angular2-multiselect field of study settings
    this.fieldDropdownSettings = {
      singleSelection: false,
      text: 'Bursary field of study',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true
    };

    // angular2-multiselect course to study settings
    this.courseDropdownSettings = {
      singleSelection: false,
      text: 'Eligible courses',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true
    };

    this.editState = true;
  }

  edit(): void {
    this.editState = !this.editState;
  }


  // todo: should the client be allowed to edit existing bursary form fields? ask Carla
  // update bursary details using the bursary id
  updateBursary(): void {
    this.alertMessage = '';

    // temp variable to hold the field values for the drop down
    const tempFields = this.bursary.field;
    this.bursary.field = [].map((a: any) => a.itemName);

    // assign string values to applicableFields arrays
    // temp variable to hold the sub field values in the right format for the drop down
    const tempApplicableFields = this.bursary.applicableFields;
    this.bursary.applicableFields = this.bursary.applicableFields.map((b: any) => b.itemName);

    // update current bursary
    this.bursaryService.updateBursary(this.bursaryId, this.bursary);
    this.alertMessage = 'You have successfully updated this bursary.'; // update feedback text fot user

    // reassign field values incase user wants to change them again
    this.bursary.field = tempFields;
    this.bursary.applicableFields = tempApplicableFields;

  }

}
