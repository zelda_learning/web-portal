import { Component, OnInit, ViewChild } from '@angular/core';
import { Bursary } from '../models/Bursary';
import { BursaryService} from './bursary.service';
import {  Router , ActivatedRoute } from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-bursary',
  templateUrl: './bursary.component.html',
  styleUrls: ['./bursary.component.css']
})
export class BursaryComponent implements OnInit {
  @ViewChild('bursaryForm') form: any;

  edit: boolean = false;
  bursaryId: string;
  selectedFieldOptions = [];
  selectedApplicableFieldOptions = [];

  selectedTab: string = 'one'; // sets which tab is shown

  bursary: Bursary = {
    id: '',
    clientId: '',
    title: '',
    bursaryUrl: '',
    field: {},
    applicableFields: [],
    subtitle: '',
    applicationProcess: '',
    supportProvided: '',
    requirements: '',
    closingDate: new Date(),
    openingDate: new Date(),
    sections: 0,
    internalApplicationForm: true
  };

  fieldOptions = [];
  stringFieldOptions = []; // array of string of the selected fields
  applicableFieldsOptions = [];
  stringApplicableFieldOptions = [];  // array of string of the selected sub fields
  fieldDropdownSettings = {};
  courseDropdownSettings = {};

  customFields = []; // form fields for user to choose from
  customFieldsSettings = {}; // form fields for user to choose from settings
  customFieldDescription: string = ''; // description of added form field
  customFieldPosition: number; // position of added form field
  selectedCustomField = []; // currently selected custom form field for the multiselect
  orderedFields = []; // ordered fields to be displayed
  orderedFieldsLength = this.orderedFields.length;
  editing: boolean = false; // true when user is editing a field
  customFieldName: string = ''; // field name of currently selected field for editing
  selectedField: object; // currently selected custom form field
  previewModalOpen: boolean = false; // state of preview modal

  alertMessage: string = ''; // alert Message
  alertType: string = ''; // alert type
  alertColorCLass: string = '' ; // colorClass


  descriptionOpen: boolean = false;
  applicationProcessOpen: boolean;
  requirementsOpen: boolean = false;
  supportOpen: boolean = false;



  constructor(
    private bursaryService: BursaryService,
    private activeRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {

    this.bursaryId = this.activeRoute.snapshot.params['id'];
    if (this.bursaryId) {

      this.edit = true;
      this.getBursary();
      this.getApplicationFormFields();

    }



    this.bursary.clientId = this.bursaryService.clientId;
    this.initialisations();
  }


  toggleWysiwyg(fieldName: string) { this[fieldName] = !this[fieldName]; }

  // close the alert container
  closeAlert() {
    this.alertMessage = ''; // alert Message
    this.alertType = ''; // alert type
    this.alertColorCLass = '' ; // colorClass
  }

  // triggered whenever a tab is changed
  changeTab(tab: string) {

    this.selectedTab = tab;

  }

  // triggered when user adds a new field
  addField() {

    console.clear();
    console.log(' \naddField called ');



    // add field option to preview if fields are filled in
    if (this.selectedCustomField !== [] && this.customFieldDescription !== '') {

      // add new item to array of fields
      this.orderedFields.push({
        field: this.selectedCustomField[0],
        itemName : this.selectedCustomField[0].itemName ,
        description : this.customFieldDescription,
        position : this.orderedFieldsLength + 1
      });

      // update ordered fields length
      this.orderedFieldsLength = this.orderedFields.length;


      if (this.selectedCustomField[0].itemName !== 'Other') {
        // remove selected field from customFieldOptions
        const newCustomFields = [];
        this.customFields.forEach((field) => {


          if (this.selectedCustomField[0].id !== field.id) {
            newCustomFields.push(field);
          }

        });

        this.customFields = newCustomFields;
      }


      // reset fields values
      this.selectedCustomField = [];
      this.customFieldDescription = '';
    }

  }

  // move custom field up
  moveUp(selectedField) {

    if (selectedField.position !== 1) {
      const orderedFieldsCopy = [...this.orderedFields]; // make copy of ordered list

      const selectedFieldNewIndexPosition = selectedField.position - 2;

      // also acts as the index position of the element which will replace the one that was moved
      const selectedFieldNewPosition = selectedField.position - 1;

      // shift position of current element
      orderedFieldsCopy[selectedFieldNewIndexPosition] = {...selectedField, position: selectedFieldNewPosition};

      // shift position of element before the selected element
      orderedFieldsCopy[selectedFieldNewPosition] = {...this.orderedFields[selectedFieldNewIndexPosition],
        position: selectedField.position };

      this.orderedFields = orderedFieldsCopy;

    }

  }

  // move custom field down
  moveDown(selectedField) {

    if (selectedField.position !== this.orderedFieldsLength) {

      const orderedFieldsCopy = [...this.orderedFields]; // make copy of ordered list

      const selectedFieldNewIndexPosition = selectedField.position ; // array index position of selected element

      const selectedFieldNewPosition = selectedField.position + 1; // new position value of selected field

      // index value of element that was previously, the next
      const adjacentElementNewIndex = selectedField.position - 1;

      // shift position of selected field
      orderedFieldsCopy[selectedFieldNewIndexPosition] = {...selectedField, position: selectedFieldNewPosition};

      // shift position of element before the selected element
      orderedFieldsCopy[adjacentElementNewIndex] = {...this.orderedFields[selectedFieldNewIndexPosition],
        position: selectedField.position };

      this.orderedFields = orderedFieldsCopy;

    }

  }

  // delete field
  deleteField(selectedField) {

    console.clear();
    console.log('deleteField Called');

    const orderedFieldsCopy = [...this.orderedFields ];
    console.log('1', orderedFieldsCopy);

    // remove selected field
    orderedFieldsCopy.splice(selectedField.position - 1 , 1);

    console.log('2', orderedFieldsCopy);

    // update list length
    this.orderedFieldsLength = orderedFieldsCopy.length;

    // update position values
    const newOrderedFields = [];
    orderedFieldsCopy.forEach((field, i) => {

      newOrderedFields[i] = {...field, position: i + 1 };

    });


    // reassign list to display
    this.orderedFields = newOrderedFields;

    // update list to choose from, by adding the field back
    const newCustomFieldsCopy = [ selectedField.field, ...this.customFields ];
    this.customFields = newCustomFieldsCopy;

  }

  // edit field, populate the editing field when user clicks on edit
  editField(selectedField) {

    console.clear();
    console.log('editField Called');

    this.editing = true;

    // update values to be edited
    this.selectedField = selectedField;
    this.customFieldDescription = selectedField.description;
    this.customFieldPosition = selectedField.position;
    this.customFieldName = selectedField.field.itemName;

  }

  // triggered when user clicks on the Update field button
  doneEditing() {

    console.clear();
    console.log('doneEditing Called');

    // create a copy of displayed fields
    const orderedFieldsCopy = [...this.orderedFields];

    // update the description of the field using the array index calculated from the position variable
    orderedFieldsCopy[this.customFieldPosition - 1] = {...this.selectedField,
      description: this.customFieldDescription};


    // reassign updated value to be displayed
    this.orderedFields = orderedFieldsCopy;

    // clear values when done editing
    this.editing = false;
    this.customFieldDescription = '';
    this.selectedCustomField = [];


  }


  getApplicationFormFields() {


    // custom bursary fields
    this.customFields = [

      { 'id': 1, 'itemName': 'First Name', 'section': 'a' , 'fieldName': 'firstname'},
      { 'id': 2, 'itemName': 'Surname', 'section': 'a' , 'fieldName': 'surname'},
      { 'id': 3, 'itemName': 'Gender', 'section': 'a' , 'fieldName': 'gender'},
      { 'id': 4, 'itemName': 'Address', 'section': 'a' , 'fieldName': 'address'},
      { 'id': 5, 'itemName': 'School Grade', 'section': 'a' , 'fieldName': 'grade'},
      { 'id': 6, 'itemName': 'Mark Average', 'section': 'a' , 'fieldName': 'average'},
      { 'id': 7, 'itemName': 'School name', 'section': 'a' , 'fieldName': 'school'},

      { 'id': 8, 'itemName': 'Awards', 'section': 'a' , 'fieldName': 'awards'},
      { 'id': 9, 'itemName': 'Clubs', 'section': 'a' , 'fieldName': 'clubs'},
      { 'id': 10, 'itemName': 'Area', 'section': 'a' , 'fieldName': 'area'},
      { 'id': 11, 'itemName': 'Cell Number', 'section': 'a' , 'fieldName': 'cellno'},
      { 'id': 12, 'itemName': 'Household income', 'section': 'a' , 'fieldName': 'income'},
      { 'id': 13, 'itemName': 'Career', 'section': 'a' , 'fieldName': 'career'},

      { 'id': 14, 'itemName': 'Problem', 'section': 'a' , 'fieldName': 'problem'},
      { 'id': 15, 'itemName': 'Woman', 'section': 'a' , 'fieldName': 'woman'},
      { 'id': 16, 'itemName': 'Siyavula', 'section': 'a' , 'fieldName': 'siyavula'},
      { 'id': 17, 'itemName': 'Other', 'section': 'a' , 'fieldName': 'other'},

    ];




    this.bursaryService
      .getBursaryForm(this.bursaryId, 'ApplicationFormA')
      .subscribe((form) => {
        console.log('returned form A', form);

        if (form) {

          const keys = Object.keys(form.formValues);

          keys.forEach( key => {

            const fieldName = key.split('_')[1];
            const position = key.split('_')[0];

              this.customFields.forEach((customField) => {

                if (customField.fieldName === fieldName) {

                  // add new item to array of fields
                  this.orderedFields.push({
                    field: customField,
                    itemName : customField.itemName ,
                    description : form.formValues[key],
                    position : position
                  });

                  // update ordered fields length
                  this.orderedFieldsLength = this.orderedFields.length;

                  if (customField.itemName !== 'Other') {
                    // remove selected field from customFieldOptions
                    const newCustomFields = [];
                    this.customFields.forEach((field) => {


                      if (customField.id !== field.id) {
                        newCustomFields.push(field);
                      }

                    });

                    this.customFields = newCustomFields;
                  }
                }
              });
          });

          console.log(this.orderedFields);
        }
      });

    this.bursaryService
      .getBursaryForm(this.bursaryId, 'ApplicationFormB')
      .subscribe((form) => {
        console.log('returned form B', form);

        if (form) {

          const keys = Object.keys(form.formValues);

          keys.forEach( key => {

            const fieldName = key.split('_')[1];
            const position = key.split('_')[0];

            this.customFields.forEach((customField) => {

              if (customField.fieldName === fieldName) {

                // add new item to array of fields
                this.orderedFields.push({
                  field: customField,
                  itemName : customField.itemName ,
                  description : form.formValues[key],
                  position : position
                });

                // update ordered fields length
                this.orderedFieldsLength = this.orderedFields.length;

                if (customField.itemName !== 'Other') {
                  // remove selected field from customFieldOptions
                  const newCustomFields = [];
                  this.customFields.forEach((field) => {


                    if (customField.id !== field.id) {
                      newCustomFields.push(field);
                    }

                  });

                  this.customFields = newCustomFields;
                }
              }
            });
          });

          console.log(this.orderedFields);
        }
      });

    this.bursaryService
      .getBursaryForm(this.bursaryId, 'ApplicationFormC')
      .subscribe((form) => {
        console.log('returned form C', form);

        if (form) {

          const keys = Object.keys(form.formValues);

          keys.forEach( key => {

            const fieldName = key.split('_')[1];
            const position = key.split('_')[0];

            this.customFields.forEach((customField) => {

              if (customField.fieldName === fieldName) {

                // add new item to array of fields
                this.orderedFields.push({
                  field: customField,
                  itemName : customField.itemName ,
                  description : form.formValues[key],
                  position : position
                });

                // update ordered fields length
                this.orderedFieldsLength = this.orderedFields.length;

                if (customField.itemName !== 'Other') {
                  // remove selected field from customFieldOptions
                  const newCustomFields = [];
                  this.customFields.forEach((field) => {


                    if (customField.id !== field.id) {
                      newCustomFields.push(field);
                    }

                  });

                  this.customFields = newCustomFields;
                }
              }
            });
          });

          console.log(this.orderedFields);
        }
      });


  }

  // create application form fields for section A, B, C
  createApplicationFormFields() {

    // form section field values
    const applicationFormAFields: object = {};
    const applicationFormBFields: object = {};
    const applicationFormCFields: object = {};

    // numbers used to give an ordered list of form values
    let fieldACounter: number = 0;
    let fieldBCounter: number = 0;
    let fieldCCounter: number = 0;

    // form sections
    let totalFormSections: number = 0;

    // add application forms A, B , C formFields
    this.orderedFields.forEach((fieldItem) => {

      // create applicationFormA formValues
      if (fieldItem.field.section === 'a') {

        fieldACounter += 1;
        let newFieldACounter: string = fieldACounter.toString();

        if (fieldACounter.toString().length < 2) {
          newFieldACounter = '0' + fieldACounter.toString();
        }



        applicationFormAFields[`${newFieldACounter}_${fieldItem.field.fieldName}`] = fieldItem.description;

      }

      // create applicationFormB formValues
      if (fieldItem.field.section === 'b') {

        fieldBCounter += 1;
        let newFieldBCounter: string = fieldBCounter.toString();

        if (fieldBCounter.toString().length < 2) {
          newFieldBCounter = '0' + fieldBCounter.toString();
        }



        applicationFormBFields[`${newFieldBCounter}_${fieldItem.field.fieldName}`] = fieldItem.description;

      }

      // create applicationFormC formValues
      if (fieldItem.field.section === 'c') {

        fieldCCounter += 1;
        let newFieldCCounter: string = fieldCCounter.toString();

        if (fieldCCounter.toString().length < 2) {
          newFieldCCounter = '0' + fieldCCounter.toString();
        }



        applicationFormCFields[`${newFieldCCounter}_${fieldItem.field.fieldName}`] = fieldItem.description;

      }

    });

    // update total number of form Sections variable
    if ( fieldACounter > 0) {
      totalFormSections += 1;
    }

    if ( fieldBCounter > 0) {
      totalFormSections += 1;
    }

    if ( fieldCCounter > 0) {
      totalFormSections += 1;
    }


    return { totalFormSections, applicationFormAFields, applicationFormBFields, applicationFormCFields };


  }

  // validate form
  formValidation() {

    // validation checks
    const {title, bursaryUrl, subtitle, applicationProcess, supportProvided, requirements } =  this.bursary;
    let valid = true;

    if (title === '') {
      valid = false;
    }

    if (bursaryUrl === '') {
      valid = false;
    }

    if (subtitle === '') {
      valid = false;
    }

    if (applicationProcess === '') {
      valid = false;
    }

    if (supportProvided === '') {
      valid = false;
    }

    if (requirements === '') {
      valid = false;
    }


    return valid;

  }

  // on submit button
  onSaveOrPublish(form: NgForm, draftOrPublish: string, navigate: string) {
    if(navigate){
      this.changeTab(navigate);
      // todo: alert user that a draft has been saved
    }
    console.clear();

    // todo: allow user to only see discard button if published

    console.log('onSubmit called with form: ', form, draftOrPublish);

    if (draftOrPublish === 'draft') {
      this.bursary.draft = true;
      this.bursary.published = false;
    } else if (draftOrPublish === 'publish') {
      this.bursary.published = true;
      this.bursary.draft = false;
    }

    // form validation checks
    const valid = this.formValidation();




      // get application form field values for different sections
      // first check if user added any fields
      if (this.orderedFieldsLength < 1) {

        console.log('Form not valid, Missing fields');

        this.alertType = 'Error'; // alert Message
        this.alertMessage = 'Form not valid, Missing section questions.'; // alert type
        this.alertColorCLass = 'w3-pale-red' ; // colorClass

      } else {

        const { totalFormSections, applicationFormAFields,
          applicationFormBFields, applicationFormCFields } = this.createApplicationFormFields();

        // update bursary sections variable
        this.bursary.sections = totalFormSections;

        // assign string values in the fields array
        // temp variable to hold the field values for the drop down

        this.selectedFieldOptions.forEach((field) => {

          this.bursary.field[field.itemName] = true;

        });


        // assign string values to applicableFields arrays
        // temp variable to hold the sub field values in the right format for the drop down
        const tempApplicableFields = this.bursary.applicableFields;
        this.bursary.applicableFields = this.bursary.applicableFields.map((b: any) => b.itemName);


        console.log(this.bursary, applicationFormAFields, applicationFormBFields, applicationFormCFields);


        if (this.edit) {

          // update id of bursary
          this.bursary.id = this.bursaryId;
          this.bursaryService.updateBursary(this.bursaryId, this.bursary);

          // if successful then add the bursary sections and formValues
          // section A
          this.bursaryService.addApplicationForm(this.bursaryId, applicationFormAFields, 'ApplicationFormA')
            .catch((error) => {
              console.log('Error updating section A: ', error);

              this.alertType = 'Error'; // alert Message
              this.alertMessage = 'Could not update bursary form sections, please edit the bursary later.'; // alert type
              this.alertColorCLass = 'w3-pale-red' ; // colorClass



            });

          // section B
          this.bursaryService.addApplicationForm(this.bursaryId, applicationFormBFields, 'ApplicationFormB')
            .catch((error) => {
              console.log('Error updating section B: ', error);

              this.alertType = 'Error'; // alert Message
              this.alertMessage = 'Could not update bursary form sections, please edit the bursary later.'; // alert type
              this.alertColorCLass = 'w3-pale-red' ; // colorClass

            });

          // section C
          this.bursaryService.addApplicationForm(this.bursaryId, applicationFormCFields, 'ApplicationFormC')
            .catch((error) => {
              console.log('Error updating section C: ', error);
              this.alertType = 'Error'; // alert Message
              this.alertMessage = 'Could not update bursary form sections, please edit the bursary later.'; // alert type
              this.alertColorCLass = 'w3-pale-red' ; // colorClass

            });



          // reset bursary object
          this.bursary = {
            id: '',
            clientId: '',
            title: '',
            bursaryUrl: '',
            field: {},
            applicableFields: [],
            subtitle: '',
            applicationProcess: '',
            supportProvided: '',
            requirements: '',
            closingDate: new Date(),
            openingDate: new Date(),
            sections: 0,
            internalApplicationForm: true
          };



          this.alertType = 'Success!'; // alert type
          this.alertMessage = 'Bursary successfully added'; // alert message
          this.alertColorCLass = 'w3-pale-green' ; // colorClass

          console.log('successfully edited bursary');

        } else {
          // add bursary to Bursaries Collection
          this.bursaryService.addBursary(this.bursary)
            .then(res => {

              // @ts-ignore
              const bursaryId = res.id; // application form id todo: reroute to manage current
              // @ts-ignore
              console.log('add bursary response: ', res.id );

              // update id of bursary
              this.bursary.id = bursaryId;
              this.bursaryService.updateBursary(bursaryId, this.bursary);

              // if successful then add the bursary sections and formValues
              // section A
              this.bursaryService.addApplicationForm(bursaryId, applicationFormAFields, 'ApplicationFormA')
                .catch((error) => {
                  console.log('Error updating section A: ', error);

                  this.alertType = 'Error'; // alert Message
                  this.alertMessage = 'Could not update bursary form sections, please edit the bursary later.'; // alert type
                  this.alertColorCLass = 'w3-pale-red' ; // colorClass



                });

              // section B
              this.bursaryService.addApplicationForm(bursaryId, applicationFormBFields, 'ApplicationFormB')
                .catch((error) => {
                  console.log('Error updating section B: ', error);

                  this.alertType = 'Error'; // alert Message
                  this.alertMessage = 'Could not update bursary form sections, please edit the bursary later.'; // alert type
                  this.alertColorCLass = 'w3-pale-red' ; // colorClass

                });

              // section C
              this.bursaryService.addApplicationForm(bursaryId, applicationFormCFields, 'ApplicationFormC')
                .catch((error) => {
                  console.log('Error updating section C: ', error);
                  this.alertType = 'Error'; // alert Message
                  this.alertMessage = 'Could not update bursary form sections, please edit the bursary later.'; // alert type
                  this.alertColorCLass = 'w3-pale-red' ; // colorClass

                });


              this.router.navigate([`bursary/${this.bursary.id}`]);

              this.alertType = 'Success!'; // alert type
              this.alertMessage = 'Bursary successfully added'; // alert message
              this.alertColorCLass = 'w3-pale-green' ; // colorClass

            })
            .catch(err => {
              console.log('add bursary error: ', err);

              this.alertType = 'Error'; // alert Message
              this.alertMessage = 'Oops something went wrong please try again.'; // alert type
              this.alertColorCLass = 'w3-pale-red' ; // colorClass

              this.bursary.applicableFields = tempApplicableFields;
            });
        }

      }

  }

  onDiscard() {
      this.bursaryService.deleteBursary(this.bursaryId);
      this.router.navigate([`bursary/new`]);
  }

  weeksBetween( date1, date2 ) {
    console.clear();
    console.log(date1, date2, 'dates received');
    // Get 1 day in milliseconds
    const one_week = 1000 * 60 * 60 * 24 * 7;

    // Convert both dates to milliseconds
    const date1_ms = date1.getTime();
    const date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    const difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_week);
  }


  getBursary() {

      if (this.bursaryId === 'new') {

        this.router.navigate(['/bursary/new'])
          .catch((err) => console.log(err));

      } else {
        this.bursaryService.getBursary(this.bursaryId)
          .subscribe(data => {
          if (data) {

            console.log('Success fetching bursary to edit ', data);

            this.bursary = data;


            // @ts-ignore
            // todo: make sure field is an array
            const keys = Object.keys(data.field);


            if ( keys.length > 0 ) {


              // set field options using field options from database
              keys.forEach(databaseField => {

                this.fieldOptions.forEach((fieldOption) => {

                  if (fieldOption.itemName === databaseField) {
                    this.selectedFieldOptions.push(fieldOption);
                  }
                });

              });

            } else {
              this.selectedFieldOptions = [{ 'id': 0, 'itemName': 'All'}];
            }





            // set applicableField options using applicable field option from database
            data.applicableFields.forEach(databaseApplicableField => {

              this.applicableFieldsOptions.forEach((localApplicableField) => {

                if (localApplicableField.itemName === databaseApplicableField) {
                  this.selectedApplicableFieldOptions.push(localApplicableField);
                }
              });

            });
            // populate the data after loop finishes
            this.bursary.applicableFields = this.selectedApplicableFieldOptions;

          } else {
            this.router.navigate(['/bursary/new'])
              .catch((err) => console.log(err));
          }
        });
      }


  }

  initialisations() {
    this.orderedFields = [
      {
        field: { 'id': 1, 'itemName': 'First Name', 'section': 'a' , 'fieldName': 'firstname'},
        itemName : 'First Name' ,
        description : 'First Name',
        position : '01'
      },

      {
        field: { 'id': 2, 'itemName': 'Surname', 'section': 'a' , 'fieldName': 'surname'},
        itemName : 'Surname' ,
        description : 'Surname',
        position : '02'
      },
      {
        field: { 'id': 3, 'itemName': 'Gender', 'section': 'a' , 'fieldName': 'gender'},
        itemName : 'Gender' ,
        description : 'Gender',
        position : '03'
      },

      {
        field: { 'id': 4, 'itemName': 'Address', 'section': 'a' , 'fieldName': 'address'},
        itemName : 'Address' ,
        description : 'Address',
        position : '04'
      },

      {
        field: { 'id': 5, 'itemName': 'School Grade', 'section': 'a' , 'fieldName': 'grade'},
        itemName : 'School Grade' ,
        description : 'Current School Grade',
        position : '05'
      },

      {
        field: { 'id': 6, 'itemName': 'Mark Average', 'section': 'a' , 'fieldName': 'average'},
        itemName : 'Mark Average' ,
        description : 'School mark average',
        position : '06'
      },

      {
        field: { 'id': 7, 'itemName': 'School name', 'section': 'a' , 'fieldName': 'school'},
        itemName : 'School name' ,
        description : 'School name',
        position : '07'
      },

      {
        field: { 'id': 8, 'itemName': 'Awards', 'section': 'a' , 'fieldName': 'awards'},
        itemName : 'Awards' ,
        description : 'Have you won any awards',
        position : '08'
      },

      {
        field: { 'id': 9, 'itemName': 'Clubs', 'section': 'a' , 'fieldName': 'clubs'},
        itemName : 'Clubs' ,
        description : 'Clubs you have joined',
        position : '09'
      },

      {
        field: { 'id': 10, 'itemName': 'Area', 'section': 'a' , 'fieldName': 'area'},
        itemName : 'Area' ,
        description : 'Area classification e.g. suburb',
        position : '10'
      },

      {
        field: { 'id': 11, 'itemName': 'Cell Number', 'section': 'a' , 'fieldName': 'cellno'},
        itemName : 'Cell Number' ,
        description : 'Cell no.',
        position : '11'
      },

      {
        field:  { 'id': 12, 'itemName': 'Household income', 'section': 'a' , 'fieldName': 'income'},
        itemName : 'Household income' ,
        description : 'What is your gross household income',
        position : '12'
      },

      {
        field: { 'id': 13, 'itemName': 'Career', 'section': 'a' , 'fieldName': 'career'},
        itemName : 'Career' ,
        description : 'What career path do you wish to pursue',
        position : '13'
      },

      {
        field: { 'id': 14, 'itemName': 'Problem', 'section': 'a' , 'fieldName': 'problem'},
        itemName : 'Problem' ,
        description : 'Which problem would you like to solve in the world',
        position : '14'
      },

      {
        field: { 'id': 15, 'itemName': 'Woman', 'section': 'a' , 'fieldName': 'woman'},
        itemName : 'Woman' ,
        description : 'Which woman inspires you the most?',
        position : '15'
      },

      {
        field: { 'id': 16, 'itemName': 'Siyavula', 'section': 'a' , 'fieldName': 'siyavula'},
        itemName : 'Siyavula' ,
        description : 'Are you on Siyavula',
        position : '16'
      },

      {
        field: { 'id': 17, 'itemName': 'Other', 'section': 'a' , 'fieldName': 'other'},
        itemName : 'Other' ,
        description : 'Is there anything else you’d like to tell us about yourself?',
        position : '17'
      }

    ];

    this.orderedFieldsLength = this.orderedFields.length;

    // custom bursary fields
    this.customFields = [
      { 'id': 17, 'itemName': 'Other', 'section': 'a' , 'fieldName': 'other'},
    ];


    this.fieldOptions = [
      { 'id': 0, 'itemName': 'All'},
      { 'id': 1, 'itemName': 'Accounting'},
      { 'id': 2, 'itemName': 'Commerce' },
      { 'id': 3, 'itemName': 'Finance' },
      { 'id': 4, 'itemName': 'Humanities' },
      { 'id': 5, 'itemName': 'Law'},
      { 'id': 6, 'itemName': 'Mathematics' },
      { 'id': 7, 'itemName': 'Science' },
      { 'id': 8, 'itemName': 'Engineering' },
      { 'id': 9, 'itemName': 'Information Technology' }
    ];


    // field options sub fields
    this.applicableFieldsOptions = [
      { 'id': 0, 'itemName': 'All'},
      { 'id': 1, 'itemName': 'Accounting', 'parentID' : 3 },
      { 'id': 2, 'itemName': 'Actuarial Science', 'parentID' : 3 },
      { 'id': 3, 'itemName': 'Business, Management and Administration', 'parentID' : 3 },
      { 'id': 4, 'itemName': 'Business Science', 'parentID' : 3 },
      { 'id': 5, 'itemName': 'Economics' , 'parentID' : 3},
      { 'id': 6, 'itemName': 'Finance' , 'parentID' : 3},
      { 'id': 7, 'itemName': 'Information Systems', 'parentID' : 3 },
      { 'id': 8, 'itemName': 'Marketing' , 'parentID' : 3},
      { 'id': 9, 'itemName': 'PPE - Politics, Philosophy and Economics', 'parentID' : 3 },
      { 'id': 10, 'itemName': 'Architecture', 'parentID' : 4 },
      { 'id': 11, 'itemName': 'Chemical Engineering', 'parentID' : 4 },
      { 'id': 12, 'itemName': 'Civil Engineering', 'parentID' : 4 },
      { 'id': 13, 'itemName': 'Construction Studies', 'parentID' : 4 },
      { 'id': 14, 'itemName': 'Computer Engineering', 'parentID' : 4 },
      { 'id': 15, 'itemName': 'Electrical Engineering', 'parentID' : 4 },
      { 'id': 16, 'itemName': 'Geomatics', 'parentID' : 4 },
      { 'id': 17, 'itemName': 'Mechanical Engineering', 'parentID' : 4 },
      { 'id': 18, 'itemName': 'Medicine', 'parentID' : 5 },
      { 'id': 19, 'itemName': 'Pharmaceutical Science', 'parentID' : 5 },
      { 'id': 20, 'itemName': 'Nursing', 'parentID' : 5 },
      { 'id': 21, 'itemName': 'Arts and Culture', 'parentID' : 7 },
      { 'id': 22, 'itemName': 'Education', 'parentID' : 7 },
      { 'id': 23, 'itemName': 'History', 'parentID' : 7 },
      { 'id': 24, 'itemName': 'Languages', 'parentID' : 7},
      { 'id': 25, 'itemName': 'Literature', 'parentID' : 7 },
      { 'id': 26, 'itemName': 'Media Studies', 'parentID' : 7 },
      { 'id': 27, 'itemName': 'Philosophy', 'parentID' : 7 },
      { 'id': 28, 'itemName': 'Politics', 'parentID' : 7 },
      { 'id': 29, 'itemName': 'Psychology', 'parentID' : 7 },
      { 'id': 30, 'itemName': 'Social Sciences', 'parentID' : 7 },
      { 'id': 31, 'itemName': 'Applied Mathematics and Mathematics', 'parentID' : 9 },
      { 'id': 32, 'itemName': 'Astronomy and Astrophysics', 'parentID' : 9 },
      { 'id': 33, 'itemName': 'Biology' , 'parentID' : 9},
      { 'id': 34, 'itemName': 'Chemistry', 'parentID' : 9 },
      { 'id': 35, 'itemName': 'Computer Science', 'parentID' : 9 },
      { 'id': 36, 'itemName': 'Environmental Sciences' , 'parentID' : 9},
      { 'id': 37, 'itemName': 'Geology' , 'parentID' : 9},
      { 'id': 38, 'itemName': 'Information Technology', 'parentID' : 9 },
      { 'id': 39, 'itemName': 'Physics' , 'parentID' : 9},
      { 'id': 40, 'itemName': 'Statistics', 'parentID' : 9 },
      { 'id': 41, 'itemName': 'Veterinary Science', 'parentID' : 9 },
    ];

    // angular2-multiselect field of study settings
    this.fieldDropdownSettings = {
      singleSelection: false,
      text: 'Fields of study',
      enableSearchFilter: true,
      displayAllSelectedText: true

    };

    // angular2-multiselect course to study settings
    this.courseDropdownSettings = {
      singleSelection: false,
      text: 'Eligible courses',
      enableSearchFilter: true,
      displayAllSelectedText: true

    };


    // angular2-multiselect custom select
    this.customFieldsSettings = {
      singleSelection: true,
      text: 'Select Field Option',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true
    };

  }

}
