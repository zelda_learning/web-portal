import { Component, OnInit } from '@angular/core';
import { BursaryService } from '../bursary.service';
import { Bursary } from '../../models/Bursary';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {
  bursaryId: string;
  bursary: Bursary;

  constructor(private bursaryService: BursaryService,
    private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.bursaryId =  this.route.snapshot.params['id'];
  }

  deleteBursary() {
    this.bursaryService.deleteBursary(this.bursaryId);
    console.log('deleting bursary..');
  }

}
