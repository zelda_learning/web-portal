import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// keep all for now but check need to import all
import { CKEditorModule } from 'ng2-ckeditor';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
 import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { BursaryRoutingModule } from './bursary-routing.module';
import { BursaryComponent } from './bursary.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';
import { PreviewComponent } from './preview/preview.component';

import { BursaryService } from './bursary.service';

@NgModule({
  imports: [
    CommonModule,
    BursaryRoutingModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AngularMultiSelectModule,
    CKEditorModule,
    FormsModule
  ],
  declarations: [
    BursaryComponent,
    EditComponent,
    DeleteComponent,
    PreviewComponent
  ],
  providers : [
    BursaryService
  ]
})
export class BursaryModule { }
