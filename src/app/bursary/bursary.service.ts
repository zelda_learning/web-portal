import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable, of } from 'rxjs';
import { Bursary, BursaryForm, BursaryMetaData } from '../models/Bursary';
import { map } from 'rxjs/operators';
import { resolve } from 'url';
import { reject } from '../../../node_modules/@types/q';
import {ApplicationForm} from '../models/ApplicationForm';


@Injectable()
export class BursaryService {
  bursariesCollection: AngularFirestoreCollection<Bursary>;
  bursaryDocument: AngularFirestoreDocument<Bursary>;
  bursaries: Observable<Bursary[]>;
  bursary: Observable<Bursary>;

  bursaryMetadataDocument: AngularFirestoreDocument<BursaryMetaData>;
  bursaryMetadata: Observable<BursaryMetaData>;

  BursaryFormDocument: AngularFirestoreDocument;
  BursaryForm: Observable<BursaryForm>;


// todo: there is currently no standard model, can come back to update later
  applicationFormDocument: AngularFirestoreDocument;

  clientId: string;

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) {

    // todo:  fix this, on a fresh login, this doesn't exist
    if (localStorage.getItem('clientId') != null) {

      this.clientId = localStorage.getItem('clientId');

      // subscribe and listen for changes made to the bursaries collection
      this.bursariesCollection = this.afs.collection('Bursaries', ref => ref.where('clientId', '==', this.clientId));
      this.bursaries = this.bursariesCollection.snapshotChanges().pipe(map(changes => {
        return changes.map(action => {
          const bursaryData = action.payload.doc.data() as Bursary;
          bursaryData.id = action.payload.doc.id;

          if (action.payload.doc.data().closingDate) {
            // @ts-ignore
            bursaryData.closingDate = action.payload.doc.data().closingDate.toDate();
          }

          if (action.payload.doc.data().openingDate) {
            // @ts-ignore
            bursaryData.openingDate = action.payload.doc.data().openingDate.toDate();

          }

          return bursaryData;
        });
      }));

    }


  }

  getBursaries(): Observable<Bursary[]> {
    this.bursaries = this.bursariesCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(action => {
        const bursaryData = action.payload.doc.data() as Bursary;

        if (action.payload.doc.data().closingDate) {
          // @ts-ignore
          bursaryData.closingDate = action.payload.doc.data().closingDate.toDate();
        }

        if (action.payload.doc.data().openingDate) {
          // @ts-ignore
          bursaryData.openingDate = action.payload.doc.data().openingDate.toDate();

        }
        return bursaryData;
      });
    }));

    return this.bursaries;

  }

  getClientId() {
    return this.clientId;
  }

  getBursary(id: string): Observable<Bursary> {

    this.bursaryDocument = this.bursariesCollection.doc<Bursary>(`${id}`);
    this.bursary = this.bursaryDocument.valueChanges().pipe(map(data => {
      if (data && data.closingDate) {
        // @ts-ignore
        data.closingDate = data.closingDate.toDate();
        // @ts-ignore
        data.openingDate = data.openingDate.toDate();
        return data as Bursary;
      } else {
        return data;
      }
    }));
    return this.bursary;
  }

  // add bursary to Bursaries collection
  addBursary(newBursary: Bursary) {

    return new Promise((resolve, reject) => {
      this.bursariesCollection = this.afs.collection('Bursaries');
      this.bursariesCollection.add(newBursary)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
    });
  }

  deleteBursary(id: string) {
    // todo: some errror handling and feedback here
    this.afs.doc(`Bursaries/${id}`).delete();
    this.afs.doc(`ApplicationFormA/${id}`).delete();
    this.afs.doc(`ApplicationFormB/${id}`).delete();
    this.afs.doc(`ApplicationFormC/${id}`).delete();
  }

  updateBursary(id: string, bursary: Bursary) {
    this.bursaryDocument = this.afs.doc(`Bursaries/${id}`);
    this.bursaryDocument.update(bursary);
  }



  // addApplicationForm A, B, C
  addApplicationForm(id: string, formValues: object, formName: string) {

    return new Promise((resolve, reject) => {

      this.applicationFormDocument = this.afs.doc(`${formName}/${id}`);
      this.applicationFormDocument.set({'formValues': formValues})
      .then((res) => {
        // @ts-ignore
        resolve(res); })
      .catch((error) => { reject(error); });

    });


  }


  getBursaryForm(id: string, formName: string): Observable<ApplicationForm> {

    this.BursaryFormDocument = this.afs.doc(`${formName}/${id}`);

    this.BursaryForm = this.BursaryFormDocument.valueChanges().pipe(map(data => data));
    return this.BursaryForm;
  }

  getBursaryMetadata(clientId: string , bursaryId: string) {

    this.bursaryMetadataDocument = this.afs.doc(`Clients/${clientId}/bursaries/${bursaryId}`);
    console.log(`Clients/${clientId}/bursaries/${bursaryId}`);
    this.bursaryMetadata = this.bursaryMetadataDocument.valueChanges();
    return this.bursaryMetadata;
  }

  updateBursaryMetaData(clientId: string, bursaryId: string, {acceptMessage, rejectMessage} ) {
    this.bursaryMetadataDocument = this.afs.doc(`Clients/${clientId}/bursaries/${bursaryId}`);
    this.bursaryMetadataDocument
      .update({acceptMessage, rejectMessage})
      .then((data) => console.log('Success', data))
      .catch((err) => console.log('opps', err));
  }

  uploadPicture(bursaryId:string, picture:any, extension:string){
    this.storage.upload(`Bursaries/${bursaryId}.${extension}` ,picture);
  }



}
