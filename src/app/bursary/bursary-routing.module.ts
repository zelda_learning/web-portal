import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BursaryComponent } from './bursary.component';

const routes: Routes = [
  {path : 'new' , component : BursaryComponent},
  {path : ':id', component : BursaryComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BursaryRoutingModule { }
